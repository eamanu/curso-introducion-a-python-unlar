import sys
import os

class Persona:
    # no se olviden del init  para definir los atributos
    #def __init__(self, nombre, apellido, edad, telefono, mail):
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.edad = None
        self.telefono = None
        self.mail = None
        self.campo_edicion = None
        self.nuevo_dato = None

    def agregar_datos(self):  # importante
        # guardan en atributos
        self.nombre = input("Ingresar Nombre: ")
        self.apellido = input("Ingresar Apellido: ")
        self.edad = input("Ingresar Edad: ")
        self.telefono = input("Ingresar Telefono: ")
        self.mail = input("Ingresar Mail: ")
        pass

    def editar_datos(self):
        # serian (email,campo,datonuevo) El email como dato unico para ubicar la fila. Validarlo en el alta entonces.
        # Alternativa 1: Trabajar con personas.txt dentro de este metodo, e impactar el cambio en el archivo fisico, desde aqui mismo.
        self.mail = input("Mail de la Persona a Editar: ")
        #self.campo_edicion = int(input("Nro de Campo a Editar (0=Nombre; 1=Apellido; 2=Edad; 3=Telefono; 4=Email): "))
        self.campo_edicion = int(input("Nro de Campo a Editar (0=Nombre; 1=Apellido; 2=Edad; 3=Telefono): "))
        self.nuevo_dato = input("Nuevo dato: ")
        if self.campo_edicion > 3 or self.campo_edicion < 0:
            print("Debe elegir desde el 0 al 3")
            return 0
        TextoABuscar = self.mail
        PosicionTexto = -1
        with open("personas.txt", 'r') as f:
            contenido_total_personas = f.read() # guardamos el contenido total del archivo para usarlo mas abajo.
            f.seek(0) # volvemos a la primera posicion del archivo, porque luego del read() queda al ultimo.
            for line in f: # comenzamos a recorrer las filas del archivo, para buscar en cada una, el Email ingresado. En variale line queda el contenido de la fila.
                PosicionTexto = line.find(TextoABuscar) # con find ubica la posicion en que comienza el texto buscado (Email en este caso), dentro de la fila.
                if PosicionTexto >= 0: # Si encuentra, devuelve una posicion de puntero mayor a 0.
                    # Con split generamos una matriz o array a partir de la variable line que tiene toda la fila donde se encuentra el Email buscado.
                    # Usa como separador de cada elemento de la matriz, un espacio en blanco (Esto lo definimos en el alta de la persona).
                    matriz_fila = line.split(" ")
                    matriz_fila[self.campo_edicion] = self.nuevo_dato # Dentro del elemento, de la matriz, que corresponde al campo ingresado en la variable campo_edicion, reemplazamos el dato.
                    fila_actual = matriz_fila[0] + " " + matriz_fila[1] + " " + str(matriz_fila[2]) + " " + str(
                        matriz_fila[3]) + " " + matriz_fila[4] # unir cada elemento de la matriz en una sola variable de la fila ya modificada.
                    contenido_nuevo_personas = contenido_total_personas.replace(line, fila_actual) # Pedimos el reemplazo a nivel de la variable que tiene el contenido completo del archivo (contenido_total_personas), de la fila nueva en lugar de la original.

                    with open("personas.txt",
                              'w') as f:  # hacemos una generacion de un nuevo archivo, donde le insertamos la variable contenido_nuevo_personas.
                        f.write(contenido_nuevo_personas)
                    break
        pass

    def borrar_datos(self):
        # Alternativa 1: Trabajar con personas.txt dentro de este metodo, e impactar el borrado en el archivo fisico, desde aqui mismo.
        self.mail = input("Mail de la Persona a Eliminar: ")
        TextoABuscar = self.mail
        PosicionTexto = -1
        contenido_nuevo_personas = ""
        with open("personas.txt", 'r') as f:
            for line in f: # comenzamos a recorrer las filas del archivo, para buscar en cada una, el Email ingresado. En variale line queda el contenido de la fila.
                PosicionTexto = line.find(TextoABuscar) # con find ubica la posicion en que comienza el texto buscado (Email en este caso), dentro de la fila.
                if PosicionTexto < 0: # Si no encuentra, devuelve una posicion de puntero menor a 0. Aqui podemos tomar las lineas, para ir grabando nuevamente. Sin incluir esta vez, a la fila del Email.
                   contenido_nuevo_personas = contenido_nuevo_personas + line

            with open("personas.txt",'w') as f:  # hacemos una generacion de un nuevo archivo, donde le insertamos la variable contenido_nuevo_personas.
                 f.write(contenido_nuevo_personas)
        pass

class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.contenido_nota = None
        self.nro_nota = None
        self.campo_edicion = None
        self.nuevo_dato = None

    def crear_notas(self): # importante
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()
        pass

    def editar_notas(self):
        # Alternativa 1: Trabajar con personas.txt dentro de este metodo, e impactar el cambio en el archivo fisico, desde aqui mismo.
        self.nro_nota = int(input("Nro de la Nota a Editar: "))
        self.campo_edicion = int(input("Nro de Campo a Editar (0=Nombre Nota; 1=Contenido): "))
        self.nuevo_dato = input("Nuevo dato: ")
        TextoABuscar = str(self.nro_nota) + " - "
        PosicionTexto = -1
        with open("notas.txt", 'r') as f:
            matriz_renglones = f.readlines()
            f.seek(0)
            puntero_matriz_renglones = 1
            for line in f:
                PosicionTexto = line.find(TextoABuscar)
                if PosicionTexto >= 0:
                    if self.campo_edicion == 0: # Modificar el nombre de la Nota que esta en el mismo renglon que nro de Nota.
                        puntero_matriz_renglones = puntero_matriz_renglones - 1
                        matriz_renglones[puntero_matriz_renglones] = str(self.nro_nota) + " - " + self.nuevo_dato + "\n"

                    if self.campo_edicion == 1:  # Modificar el contenido de la Nota, que esta en el renglon de abajo al de nro de Nota.
                        # saltar 1 renglon hacia abajo. Usamos la matriz_renglones con el puntero asociado, que ya viene con el renglon siguiente adonde estamos parados.
                        matriz_renglones[puntero_matriz_renglones] =  self.nuevo_dato + "\n"
                    break
                puntero_matriz_renglones = puntero_matriz_renglones + 1
        with open("notas.txt","w") as f:
            f.writelines(matriz_renglones)
        pass

    def borrar_notas(self):
        self.nro_nota = int(input("Nro de la Nota a Borrar: "))
        TextoABuscar = str(self.nro_nota) + " - "
        PosicionTexto = -1
        with open("notas.txt", 'r') as f:
            matriz_renglones = f.readlines()
            f.seek(0)
            puntero_matriz_renglones = 1
            for line in f:
                PosicionTexto = line.find(TextoABuscar)
                if PosicionTexto >= 0:
                   puntero_matriz_renglones = puntero_matriz_renglones - 1
                   matriz_renglones[puntero_matriz_renglones] = ""
                   # saltar 1 renglon hacia abajo.
                   puntero_matriz_renglones = puntero_matriz_renglones + 1
                   matriz_renglones[puntero_matriz_renglones] = ""
                   break
                puntero_matriz_renglones = puntero_matriz_renglones + 1
        with open("notas.txt","w") as f:
            f.writelines(matriz_renglones)
        pass

    def escribir_contenido_notas(self):
        self.contenido_nota = input("contenido:")
        # nombre de la nota
        pass

    def colocar_nombre_notas(self):
        self.nro_nota = int(input("nro de Nota:"))
        self.nombre_nota = input("nombre de Nota:")
        pass

class Memoria:
    # init definir los atributos
    def guardar_informacion_persona(self, persona): #importante
        nombre = persona.nombre
        apellido = persona.apellido
        edad = persona.edad
        telefono = persona.telefono
        mail = persona.mail

        with open("personas.txt", 'r') as f:
            a=f.read()
            # buscar el mail dentro de a, con manipulacion de cadenas. Otra alternativa seria hacer la busqueda con tratamiento de archivos (como esta en el metodo editar_datos de clase Persona)
            if mail in a:
                print("El Email ya esta registrado. Ingrese otro")
            else:
                with open("personas.txt", 'a') as f:
                    f.write(nombre)
                    f.write(" "+apellido)
                    f.write(" "+edad)
                    f.write(" "+telefono)
                    f.write(" "+mail)
                    #f.write(" EOF")
                    f.write("\n")
        pass

    # Alternativa 2: Crear otro metodo "Guardar_Edicion_Persona" en la Clase Memoria, y alli trabajar con personas.txt, e impactar el cambio en el archivo fisico, recien.
    """
    def guardar_edicion_fila_persona(self,persona):
        mail = persona.mail
        campo_edicion = persona.campo_edicion
        nuevo_dato = persona.nuevo_dato
        
        TextoABuscar = mail
        PosicionTexto = -1
        with open("personas.txt", 'r') as f:
            contenido_total_personas = f.read()
            f.seek(0)
            for line in f:
                PosicionTexto = line.find(TextoABuscar)
                if PosicionTexto >= 0:
                    matriz_fila = line.split(" ")
                    #print("Fila original", matriz_fila)
                    matriz_fila[campo_edicion] = nuevo_dato
                    #print("Fila modificada en posicion ",campo_edicion,matriz_fila)
                    # unir cada elemento de la matriz en una sola variable de la fila ya modificada.
                    fila_actual = matriz_fila[0] + " " + matriz_fila[1] + " " + str(matriz_fila[2]) + " " + str(
                        matriz_fila[3]) + " " + matriz_fila[4]

                    contenido_nuevo_personas = contenido_total_personas.replace(line, fila_actual)
                    break

        with open("personas.txt", 'w') as f:
            f.write(contenido_nuevo_personas)
        """

    def guardar_informacion_notas(self, nota): #importante
        nro_nota = str(nota.nro_nota)
        nombre_nota = nota.nombre_nota
        contenido_nota = nota.contenido_nota
        with open("notas.txt", 'r') as f:
            a=f.read()
            # buscar el nro_nota dentro de a, con manipulacion de cadenas. Otra alternativa seria hacer la busqueda con tratamiento de archivos (como esta en el metodo editar_datos de clase Persona)
            if nro_nota in a:
                print("El Nro de Nota ya esta registrado. Ingrese otro")
            else:
                with open("notas.txt", 'a') as f:
                    #f.write("\n")
                    f.write(nro_nota)
                    f.write(" - ")
                    f.write(nombre_nota)
                    f.write("\n")
                    f.write(contenido_nota)
                    f.write("\n")
        pass

    def mostrar_personas(self): # importantes
        with open("personas.txt", 'r') as f:
             print(f.read())
        pass

    def mostrar_notas(self): # importantes
        with open("notas.txt", 'r') as f:
            print(f.read())
        pass

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las personas")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para editar una persona")
                print("Presione 8 para editar una nota")
                print("Presione 9 para borrar una persona")
                print("Presione 10 para borrar una nota")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Editar datos de una persona...")
                    self.editar_datos_persona()
                elif seleccion == 8:
                    print("Editar datos de una nota...")
                    self.editar_datos_nota()
                elif seleccion == 9:
                    print("Borrar una persona...")
                    self.borrar_datos_persona()
                elif seleccion == 10:
                    print("Borrar una nota...")
                    self.borrar_datos_nota()
                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def editar_datos_persona(self):
        self.persona = Persona()
        self.persona.editar_datos()

    def borrar_datos_persona(self):
        self.persona = Persona()
        self.persona.borrar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def editar_datos_nota(self):
        self.nota = Notas()
        self.nota.editar_notas()

    def borrar_datos_nota(self):
        self.nota = Notas()
        self.nota.borrar_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una nota")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()
