# Ejercicio1 - Ingresar un Texto
def texto(a,b,c):
    with open("texto.txt", "w") as f:

        print(a +" "+ b +" "+ c)
        f.write(a +" "+ b +" "+ c)

a = input("primera parte: ")
b = input("segunda parte: ")
c = input("tercera parte: ")

texto(a,b,c)
##############################
# Ejercicio2 - Ingresar un numero para ver si en una operación da un resultado mayor a otro numero
def mayor_a(a,b):
    with open("mayor_a.txt", "w") as f:
        suma = a * (a + 1) / 2
        if suma > b:
            print("El calculo dio ",suma," y es mayor a ",b)
            f.write("El calculo dio "+str(suma)+" y es mayor a " + str(b))
        else:
            print("El calculo dio ", suma, " y no es mayor a ", b)
            f.write("El calculo dio " + str(suma) + " y no es mayor a " + str(b))

a = int(input("numero para calculo: "))
b = int(input("numero a comparar: "))

mayor_a(a,b)
#########################
# Ejercicio3 - numeros pares
def pares(desde=5,hasta=1000):
    with open("numero_pares.txt", "w") as f:
        # generar primero el archivo, y luego recien hacer el for y el llenado
        for i in range(desde,hasta):
            if i % 2 == 0:
               print(i)
               f.write(str(i))
               f.write("\n")

desde = int(input("Ingrese Desde: "))
hasta = int(input("Ingrese Hasta: "))

pares(desde,hasta)
#######################
# Ejercicio4 - numeros impares
def impares(desde=10,hasta=1000):
    with open("numero_impares.txt", "w") as f:
        # generar primero el archivo, y luego recien hacer el for y el llenado
        for i in range(desde,hasta):
            if i % 2 > 0:
               print(i)
               f.write(str(i))
               f.write("\n")

desde = int(input("Ingrese Desde: "))
hasta = int(input("Ingrese Hasta: "))

impares(desde,hasta)
###########################
#Ejercicio 5 - tabla
def tabla(numero=9):
    a = str(numero)+" x "
    with open("tabla.txt", "w") as f:
        for i in range(0,11):
            resultado = numero * i
            print(a, i, "=", resultado)
            f.write(a+str(i)+" = "+str(resultado))
            f.write("\n")

numero = int(input("Numero: "))

tabla(numero)
###########################
# Ejercicio 6 - Interseccion de numeros
def intersec(rango1, rango_inicial_2, rango_final_2):
    acu = 0
    for i in range(rango1):
        for j in range(rango_inicial_2, rango_final_2):
            if i == j:
                acu += 1
    print("hay ", acu, " numeros")
    with open("intersec.txt", "w") as f:
        texto = "Hay "+str(acu)+" números en la intersección"
        f.write(texto)

intersec(100, 67, 200)
############################
# Ejercicio 7 - Numeros primos entre 0 y 1000
def primos(desde,hasta):
    resultado3 = 0
    with open("primos.txt", "w") as f:
        for i in range(desde,hasta):
            acumula_no_primo = 0
            if i > 2:
                numero_anterior = i - 1
                for j in range(2,numero_anterior):
                    resultado3 = i % j
                    if resultado3 == 0:
                        acumula_no_primo = acumula_no_primo + 1

            if acumula_no_primo == 0:
               print(i,"Es Primo")

               f.write(str(i) + " Es Primo")
               f.write("\n")

desde = int(input("Numero desde: "))
hasta = int(input("Numero hasta: "))

primos(desde,hasta)
############################
# Ejercicio 5a - Numero PI
def numeropi():
	with open("ejercicio5a.txt", "w") as f:
		print("Vamos a encontrar el numero PI usando un FOR yeah!!!")
		n = 10000000
		calc = 0
		for i in range(1, n + 1):
		    calc += (-1)**(i + 1) / (2 * i - 1)

		resultado = 4 * calc
		print("El valor del numero PI es:",resultado)
		print("FIN")

		f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!")
		f.write("\n\n")
		f.write("El valor del numero PI es: "+str(resultado))
		f.write("\n\n")
		f.write("FIN")

numeropi()
############################
# Ejercicio 6a - Descuento Precio
def precio_descuento(precio,porc_descuento):
	with open("ejercicio6a.txt", "w") as f:
	    importe_descuento = (porc_descuento * precio) / 100
	    resultado = precio - importe_descuento
    	f.write("Se debe cobrar $ " + str(resultado) + " con el descuento de " + str(porc_descuento) + " %.")

precio = int(input("Precio $: "))
porc_descuento = int(input("Descuento %: "))

precio_descuento(precio,porc_descuento)
############################
# Ejercicio 7a - Numero a Meses
def meses(numero):
    if numero > 0 and numero < 13:
    	with open("ejercicio7a.txt", "w") as f:
	        if numero == 1:
	            print("Enero")
	            f.write(str(numero)+"=Enero")
	        elif numero == 2:
	            print("Febrero")
	            f.write(str(numero)+"=Febrero")
	        elif numero == 3:
	            print("Marzo")
	            f.write(str(numero)+"=Marzo")
	        elif numero == 4:
	            print("Abril")
	            f.write(str(numero)+"=Abril")
	        elif numero == 5:
	            print("Mayo")
	            f.write(str(numero)+"=Mayo")
	        elif numero == 6:
	            print("Junio")
	            f.write(str(numero)+"=Junio")
	        elif numero == 7:
	            print("Julio")
	            f.write(str(numero)+"=Julio")
	        elif numero == 8:
	            print("Agosto")
	            f.write(str(numero)+"=Agosto")
	        elif numero == 9:
	            print("Setiembre")
	            f.write(str(numero)+"=Setiembre")
	        elif numero == 10:
	            print("Octubre")
	            f.write(str(numero)+"=Octubre")
	        elif numero == 11:
	            print("Noviembre")
	            f.write(str(numero)+"=Noviembre")
	        elif numero == 12:
	            print("Diciembre")
	            f.write(str(numero)+"=Diciembre")
    else:
        print("Debe ingresar un numero entre 1 y 12")

numero = int(input("Ingrese Numero de Mes: "))
meses(numero)
