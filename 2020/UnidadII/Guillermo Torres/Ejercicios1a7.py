#--------------Ejercicio 1-----------------------

def num_pares(desde=5, hasta=1000):
    with open("Ejercicio1.txt", 'w') as f:
        for i in range(desde, hasta):
            if i%2 == 0:
                print(i)
                f.write(str(i))
                f.write('\n')

print("Ejercicio de Nros Pares\n")

desde = int(input("Ingrese el rango inicial: "))
hasta = int(input("\nIngrese el rango final: "))

num_pares(desde, hasta)

#--------------Ejercicio 2-----------------------

def num_impares(desde=5, hasta=1000):
    with open("Ejercicio2.txt", 'w') as f:
        for i in range(desde, hasta):
            if i%2 != 0:
                print(i)
                f.write(str(i))
                f.write('\n')
print("\n------------------------------------------")
print("\nEjercicio de Nros Impares\n")

desde = int(input("Ingrese el rango inicial: "))
hasta = int(input("\nIngrese el rango final: "))

num_impares(desde, hasta)

#--------------Ejercicio 3-----------------------

def calcular_tabla(n):
    with open("Ejercicio3.txt", 'w') as f:
        for i in range(11):
            print(n,"\tx\t",i,"\t","=\t",n*i)
            f.write(str(i))
            f.write("\tx\t")
            f.write(str(n))
            f.write("=\t")
            f.write(str(n * i))
            f.write("\n")

print("\n------------------------------------------")
print("\nEjercicio de Tabla Multiplicar\n")

n = int(input("Ingrese el Nro a Multiplicar: "))

calcular_tabla(n)

#--------------Ejercicio 4-----------------------

def numeros_primos(desde=2, hasta=1000):
    from math import sqrt

    with open("Ejercicio4.txt", 'w') as f:
        for i in range(desde, hasta):
            primo = True
            divisor = 2
            while primo and divisor <= int(sqrt(i)):
                if i % divisor == 0:
                    primo = False
                else:
                    divisor += 1
            if primo:
                print(i)
                f.write(str(i))
                f.write('\n')

print("\n------------------------------------------")
print("\nEjercicio de Nros Primos\n")

desde = int(input("Ingrese el rango inicial: "))
hasta = int(input("\nIngrese el rango final: "))

numeros_primos(desde, hasta)

#--------------Ejercicio 5-----------------------

def funcion_pi():

    n = 10000000
    calc = 0
    r1 = 0
    with open("Ejercicio5.txt", 'w') as f:
        for i in range(1, n + 1):
            calc += (-1) ** (i + 1) / (2 * i - 1)
            r1 = 4 * calc
        f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!\n")
        f.write("\nEl Valor del numero PI es: ")
        f.write(str(r1))
        f.write("\n\nFIN")
        print("El Valor del numero PI es: ", r1, "\n\nFIN")


print("\n------------------------------------------")

print("Vamos a encontrar el numero PI usando un FOR yeah!!!\n")

funcion_pi()

#--------------Ejercicio 6-----------------------

def calcular_porcentaje(precio, descuento):

    calculo = precio - precio * descuento/100
    with open("Ejercicio6.txt", 'w') as f:
        print("\nSe debe cobrar $", calculo, "con el descuento del", descuento, "%")
        f.write("Se debe cobrar $ ")
        f.write(str(calculo))
        f.write(" con el descuento del ")
        f.write(str(descuento))
        f.write(" % ")

print("\n------------------------------------------")
print("Calcular el Precio\n")

precio = int(input("Ingrese el Precio del Producto: $ "))
descuento = int(input("Ingrese el % de Descuento: "))

calcular_porcentaje(precio, descuento)

#--------------Ejercicio 7-----------------------


def ingresar_mes():
    with open("Ejercicio7.txt", 'w') as f:
        if n <= 1:
            print("El mes es Enero")
            f.write("Ingrese un valor: ")
            f.write(str(n))
            f.write("\nEl mes es Enero")
        elif n <= 2:
            print("El mes es Febrero")
            f.write("El mes es Febrero")
        elif n <= 3:
            print("El mes es Marzo")
            f.write("El mes es Marzo")
        elif n <= 4:
            print("El mes es Abril")
            f.write("El mes es Abril")
        elif n <= 5:
            print("El mes es Mayo")
            f.write("El mes es Mayo")
        elif n <= 6:
            print("El mes es Junio")
            f.write("El mes es Junio")
        elif n <= 7:
            print("El mes es Julio")
            f.write("El mes es Julio")
        elif n <= 8:
            print("El mes es Agosto")
            f.write("El mes es Agosto")
        elif n <= 9:
            print("El mes es Septiembre")
            f.write("El mes es Septiembre")
        elif n <= 10:
            print("El mes es Octubre")
            f.write("El mes es Octubre")
        elif n <= 11:
            print("El mes es Noviembre")
            f.write("El mes es Noviembre")
        elif n <= 12:
            print("El mes es Diciembre")
            f.write("El mes es Diciembre")
        else:
            print("El nro ingresado no corresponde a un mes")
            f.write("El nro ingresado no corresponde a un mes")

print("\n------------------------------------------")
print("Verificar el Mes\n")

n = int(input("Ingrese un valor: "))

ingresar_mes()


