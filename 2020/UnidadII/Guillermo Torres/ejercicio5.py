from math import sqrt

for i in range (2,10000):
    primo = True
    divisor = 2
    while primo and divisor <= int (sqrt(i)):
        if i % divisor == 0:
            primo = False
        else:
            divisor += 1
    if primo:
        print(i)
