from http import client

def web_response(web, response):
    if response.status == 200:
        print(web, " anda bien!!!")
    else:
        print(web, " esta caido? Error: ", response.reason)

def get_connections(web, puerto, timeout):
    con = client.HTTPConnection(web, puerto, timeout=timeout)
    con.request("GET", "/")
    response = con.getresponse()
    print(response.status, response.reason)

    data = response.read()
    print(data)

    web_response(web, response)

get_connections("www.python.org", 80, 10)
get_connections("www.google.com", 80, 10)
get_connections("www.facebook.com", 80, 10)
get_connections("www.twitter.com", 80, 10)
