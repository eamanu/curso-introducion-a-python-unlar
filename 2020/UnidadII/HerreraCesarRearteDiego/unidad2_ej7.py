#Numeros primos entre 0 y 1000
resultado3 = 0
for i in range(1,1001):
    acumula_no_primo = 0
    resultado1 = i % i
    resultado2 = i % 1
    if i > 2:
        numero_anterior = i - 1
        for j in range(2,numero_anterior):
            resultado3 = i % j
            if resultado3 == 0:
                acumula_no_primo = acumula_no_primo + 1

    if acumula_no_primo == 0:
        print(i,"Es Primo")
    #Todos los resultados, el ultimo valor si es mayor a 0, indica que no es primo (excepto los 3 primeros
    #numeros que son el 1, 2, y 3, y son primos).
    #print(i,resultado1,resultado2,resultado3,acumula_no_primo)
