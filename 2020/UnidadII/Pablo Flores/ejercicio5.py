'''Calcular los números primos entre 0 y 10.000.
El resultado esperado debe ser el siguiente.
2
3
5
7
...
...'''

for i in range(10001):
    contador = 0
    for j in range(2, i):
        if i % j == 0:
            contador +=1

    if contador > 0 :
             print("El número", i , "no es primo")
    else:
        print("El nÚmero", i , "es primo")