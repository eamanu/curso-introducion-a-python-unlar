'''Escribir la tabla del 9 en en siguiente formato, la cual es la salida esperada:
9 x 0 = 0
9 x 1 = 9
9 x 2 = 18'''

for i in range(11):
    mult = 9 * i
    print("9\tx\t",i,"=",mult)