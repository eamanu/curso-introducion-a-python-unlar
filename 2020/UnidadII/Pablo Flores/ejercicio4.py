'''Cuál es la cantidad de números que hay en la intersección entre la secuencia que
va desde el 0 hasta el 100 y el 67 hasta el 200.'''

cont = 0
for i in range(101):
    for j in range(67,201):
        if i==j:
            cont +=1
print("Hay", cont ,"numeros en la intersección entre 0 hasta el 100 y el 67 hasta el 200")