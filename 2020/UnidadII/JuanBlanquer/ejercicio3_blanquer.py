#escribir la tabla del 9 con un formato especifico 9 x 0 = 0 (hasta el 10)
'''
tabla = 9
i = 0
resultado = 0
for i in range(11):  #uso el 11 porque el cero va incluido
    resultado = tabla * i
    print("9\tx\t", i, "\t=\t", resultado)  #\t da un espacio tabulado
'''
#otro modo mas sencillo
for i in range(11):
    print("9\tx\t", i, "\t=\t", i*9)