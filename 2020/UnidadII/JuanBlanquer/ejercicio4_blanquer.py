'''Cual es la cantidad de numeros que hay en la interseccion entre la 
secuencia que va desde cero a cien y del sesenta y siete hasta el doscientos
'''
''' prueba 1 = error
for conjuntoA in range(0, 101):
    for conjuntoB in range(67, 201):
        print(conjuntoA & conjuntoB)
'''

''' prueba 2 = error
for conjuntoA in range(0, 101):
    for conjuntoB in range(67, 201):
        if (conjuntoA == conjuntoB):
            print(conjuntoA == conjuntoB)
'''            
#declarar una variable para que cuente los valores comunes
cantidad = 0
for conjuntoA in range(100):
    for conjuntoB in range(67, 200):
        if (conjuntoA == conjuntoB):
            cantidad = cantidad + 1
print(cantidad, " son los numeros comunes en los dos conjuntos.")
