#Calcular los numeros primos entre 0 y 10.000
#El resultado esperado debe ser el siguiente

for x in range(2,10001):
    for i in range(1,x+1):
        primo=True
        resto=x%i
        if resto==0 and i!=1 and i!=x:
            primo=False
            break
    if primo==True:
        print(x,"es primo")