print("Vamos a encontrar el numero PI usando un FOR yeah!!!", end='\n\n')
n = 10000000
x = 0
calc = 0

for i in range(1, n + 1):
    calc += (-1)**(i + 1) / (2 * i - 1)
resultado = 4 * calc

print("El valor del numero pi es:", resultado, end="\n\n")
print("FIN")