#Ejercicio 4 - (Lo que interprete antes de que pasaras el PDF por el whatsapp)
import numpy as np

#Numeros a comparar minimo...maximo
minimoRango1 = 0
maximoRango1 = 21 #maximo +1

#Lista con la que se va a comparar minimo...maximo
minimoRango2 = 0
maximoRango2 = 101 #maximo +1

listaUno = list(range(maximoRango1))
listaDos = np.zeros(maximoRango1)
for k in listaUno:
        for i in range(minimoRango2,maximoRango2):
            if str(k) in str(i): #transformo cada numero a string y busco el numero k dentro de la (ahora) cadena i
                listaDos[k]+=1  #si es verdadero (se encuentra k en i) le sumo 1 al elemento de la posicion en la 2da lista

print("Del " +str(minimoRango2)+ " al "+ str(maximoRango2-1))
for k in range(0,21): #Este loop es solo para formatear la salida
    print(listaUno[k]," se repite = ", int(listaDos[k]), " veces")