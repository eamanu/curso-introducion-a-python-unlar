"""
Ejercicio 4: Cúal es la cantidad de números que hay en la intersección entre la secuencia que
va desde el 0 hasta el 100 y el 67 hasta el 200.
"""

count=0
for i in range(101):
    if i in range(67,201):
        count+=1         
print("La cantidad que se repite es:",count)
