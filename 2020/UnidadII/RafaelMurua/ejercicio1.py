"""
Ejercicio 1: Escribir en pantalla una lista del 5 al 1.000 de números pares.
El resultado esperado debe ser el siguiente.
6
8
10
12
...
...

"""

print("Estos son los valores Pares entre 5 y 1000: ")
for i in range(5,1001):
    if i % 2 == 0:
        print(i)