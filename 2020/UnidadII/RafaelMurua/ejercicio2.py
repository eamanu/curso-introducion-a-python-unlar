"""
Ejercicio 2: Escribir en pantalla una lista del 5 al 1.000 de números impares.
El resultado esperado debe ser el siguiente.
5
7
9
11
...
...
"""

print("Estos son los valores Impares entre 5 y 1000:")
for i in range(5,1001):
    if i % 2 != 0:
        print(i)