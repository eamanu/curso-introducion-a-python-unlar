"""
Ejercicio 3: Escribir la tabla del 9 en en siguiente formato, la cual es la salida esperada:
9 x 0 = 0
9 x 1 = 9
9 x 2 = 18
...
...
Para escribir en el formato eserado de salida, como se muestra en el ejemplo se
debe usar un tabular, no espacios.

"""
for i in range(11):
    print("9 x",i, "=",9*i, sep=" ")