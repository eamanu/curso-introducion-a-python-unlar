print ("Vamos a encontrar el numero PI usando un FOR yeah!!!", end='\n\n') # se corrio la comilla, se cambio el 1,2,3 del end y se lo remplazo por saltos de linea y se puso la palabra print al proncipio.
n = 10000000
#x == 0 No se debe definir, porque en el programa no se utiliza la variable x y para definir se utiliza un solo signo =, por lo tanto los == se los utilizan para comparaciones entre dos valores o variables.
calc = 0 #requeria indentación
for i in range(1, n + 1):
	calc += (-1)**(i + 1) / (2 * i - 1) #requeria indentación
	resultado = 4 * calc #salto de linea
print ("El valor del numero PI es: ", resultado, '\n\nFIN!')
