import sys
import os

class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.email = None
        self.Telefono = None
        pass
    def agregar_datos(self):
        self.nombre = input("Ingrese el nombre: ")
        self.apellido = input("Ingrese el apellido: ")
        self.email = input("Ingrese el email: ")
        self.Telefono = int(input("Ingrese el numero de telefono: "))
        pass


class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.contenido = None
        pass
    def crear_notas(self):
        self.colocar_nombre_nota()
        self.colocar_contenido_nota()
    def colocar_nombre_nota(self):
        self.nombre_nota= input("colocar nombre de la nota:\n ")
    def colocar_contenido_nota(self):
        self.contenido= input("Colocar contenido de la nota:\n ")



class Memoria:
    def __init__(self):
        pass

    def guardar_informacion_persona(self, persona):
        print("Nombre: ", persona.nombre)
        print("Apellido: ", persona.apellido)
        print("Email: ", persona.email)
        print("Telefono: ", persona.Telefono)
        with open("personas.txt", 'a') as f:
            f.write(persona.nombre)
            f.write(" ")
            f.write(persona.apellido)
            f.write(" ")
            f.write(persona.email)
            f.write(" ")
            f.write('{}'.format(persona.Telefono))
            f.write(" ")
            f.write("EOF \n")

    def guardar_informacion_notas(self, nota):
        print("Nombre de la nota: ", nota.nombre_nota)
        print("Contenido de la nota: ", nota.contenido)
        with open("notas.txt", 'a') as f:
            f.write(nota.nombre_nota)
            f.write(" ")
            f.write(nota.contenido)
            f.write(" ")
            f.write("EOF \n")

    def mostrar_personas(self):
        with open("personas.txt", 'r') as f:
            print(f.read())

    def mostrar_notas(self):
        with open("notas.txt", 'r') as f:
            print(f.read())

    def eliminar_persona(self):
        nombre = input("ingrese nombre a borrar: \n")
        with open("personas.txt", 'r') as f:
            lines = f.readlines()
        with open("personas.txt", 'w') as f:
            for line in lines:
                if not nombre in line:
                    f.write(line)
        print("\nPersona borrada satisfactoriamente")

    def eliminar_nota(self):
        nombre = input("ingrese nombre de la nota a borrar: \n")
        with open("notas.txt", 'r') as f:
            lines = f.readlines()
        with open("notas.txt", 'w') as f:
            for line in lines:
                if not nombre in line:
                    f.write(line)
        print("\nNota borrada satisfactoriamente")

    def editar_persona(self):
        nombre = input("ingrese nombre a editar: \n")
        t = False
        with open('personas.txt') as f:
            if nombre in f.read():
                t= True
        if t:
            print("Ingrese de nuevo los datos:")
            n=input("ingrese el nuevo nombre: \n")
            a=input("ingrese el nuevo apellido: \n")
            e=input("ingrese el nuevo email: \n")
            tel=input("ingrese el nuevo telefono: \n")
            with open("personas.txt", 'r') as f:
                lines = f.readlines()
            with open("personas.txt", 'w') as f:
                for line in lines:
                    if nombre in line:
                        f.write(n)
                        f.write(" ")
                        f.write(a)
                        f.write(" ")
                        f.write(e)
                        f.write(" ")
                        f.write('{}'.format(tel))
                        f.write(" ")
                        f.write("EOF \n")
                    else:
                        f.write(line)
            print("\nPersona editada satisfactoriamente")
        else:
            print("\n No existe esa persona")

    def editar_nota(self):
        nombre = input("ingrese nombre de la nota a editar: \n")
        t = False
        with open('notas.txt') as f:
            if nombre in f.read():
                t= True
        if t:
            print("Ingrese de nuevo los datos:")
            n=input("ingrese el nuevo nombre de la nota: \n")
            c=input("ingrese el nuevo contenido de la nota: \n")
            with open("notas.txt", 'r') as f:
                lines = f.readlines()
            with open("notas.txt", 'w') as f:
                for line in lines:
                    if nombre in line:
                        f.write(n)
                        f.write(" ")
                        f.write(c)
                        f.write(" ")
                        f.write("EOF \n")
                    else:
                        f.write(line)
            print("\nNota editada satisfactoriamente")
        else:
            print("\n No existe esa Nota")


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para borrar una persona")
                print("Presione 8 para borrar una nota")
                print("Presione 9 para editar una persona")
                print("Presione 10 para editar una nota")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    self.eliminar_persona()
                elif seleccion == 8:
                    self.eliminar_nota()
                elif seleccion == 9:
                    self.editar_persona()
                elif seleccion == 10:
                    self.editar_nota()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()

    def eliminar_persona(self):
        self.memoria.eliminar_persona()

    def eliminar_nota(self):
        self.memoria.eliminar_nota()

    def editar_persona(self):
        self.memoria.editar_persona()

    def editar_nota(self):
        self.memoria.editar_nota()


if __name__ == '__main__':
    menu = Menu()
    menu.run()