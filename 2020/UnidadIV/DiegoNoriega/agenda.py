import sys
import os
import json


class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None

    def agregar_datos(self):
        self.nombre = input('Escriba el nombre: ')
        self.apellido = input('Escriba el apellido: ')
        self.telefono = input('Escriba el telefono: ')
        self.email = input('Escriba el email: ')


class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.contenido_nota = None

    def crear_notas(self):
        self.colocar_nombre_nota()
        self.escribir_contenido_nota()

    def colocar_nombre_nota(self):
        self.nombre_nota = input('Escriba el nombre de la nota: ')

    def escribir_contenido_nota(self):
        self.contenido_nota = input('Escriba el contenido de la nota: ')


class Memoria:
    def __init__(self):
        self.lista = []
        pass

    def guardar_informacion_de_persona(self, persona):
        nom = persona.nombre
        app = persona.apellido
        tel = persona.telefono
        em = persona.email

        dic = {"Nombre": nom, "Apellido": app, "Telefono": tel, "Email": em}
        self.lista.append(dic)

        with open('agenda.json', 'w') as file:
            json.dump(self.lista, file, indent=4)
        print('Persona guardada correctamente')

    def editar_informacion_de_persona(self):
        with open('agenda.json') as file:
            agenda = json.load(file)
        print('Los datos en agenda son: ', agenda)
        print('Existen', len(agenda), 'elementos en agenda')

        persona = input('Ingrese la persona a editar: ')
        for p in agenda:
            if p['Nombre'] == persona:
                p['Nombre'] = input('Nombre: ')
                p['Apellido'] = input('Apellido: ')
                p['Telefono'] = input('Telefono: ')
                p['Email'] = input('Email: ')
            else:
                print('La persona ingresada no existe. Vuelva a intentar')

        print('Persona modificada', agenda)
        with open('agenda.json', 'w') as file:
            json.dump(agenda, file, indent=4)

    def eliminar_informacion_de_persona(self):
        with open('agenda.json') as file:
            agenda = json.load(file)
        print('Agenda: ', agenda)

        try:
            indice = int(input('Ingrese el indice de la persona a eliminar. El indice comienza en cero (0): '))
            print('Se ha eliminado la siguiente persona: ', agenda.pop(indice))
        except IndexError:
            print("Indice fuera de rango")

        with open('agenda.json', 'w') as file:
            json.dump(agenda, file, indent=4)

    def mostrar_informacion_de_personas(self):
        with open('agenda.json') as file:
            agenda = json.load(file)
        if agenda == []:
            print('No hay personas para mostrar')
        else:
            print(agenda)

    def guardar_informacion_de_nota(self, nota):
        nom_nota = nota.nombre_nota
        cont_nota = nota.contenido_nota

        nota = {"Nombre de nota": nom_nota, "Contenido de nota": cont_nota}
        self.lista.append(nota)

        with open('nota.json', 'w') as file:
            json.dump(self.lista, file, indent=4)
        print('Nota guardada correctamente')

    def editar_informacion_de_nota(self):
        with open('nota.json') as file:
            nota = json.load(file)
        print('Los datos en nota son: ', nota)
        print('Existen', len(nota), 'elementos en la nota')

        note = input('Ingrese el nombre de la nota a editar: ')
        for n in nota:
            if n['Nombre de nota'] == note:
                n['Nombre de nota'] = input('Nombre de nota: ')
                n['Contenido de nota'] = input('Contenido de nota: ')
            else:
                print('El nombre de nota ingresado no existe. Vuelva a intentar')

        print('Nota modificada', nota)
        with open('nota.json', 'w') as file:
            json.dump(nota, file, indent=4)

    def eliminar_informacion_de_nota(self):
        with open('nota.json') as file:
            nota = json.load(file)
        print('Nota: ', nota)
        try:
            indice = int(input('Ingrese el indice de la nota a eliminar. El indice comienza en cero (0): '))
            print('Se ha eliminado la siguiente nota: ', nota.pop(indice))
        except IndexError:
            print("Indice fuera de rango")

        with open('nota.json', 'w') as file:
            json.dump(nota, file, indent=4)

    def mostrar_informacion_de_nota(self):
        with open('nota.json') as file:
            nota = json.load(file)
        if nota == []:
            print('No hay nota para mostrar')
        else:
            print(nota)


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'nt' else 'clear')
                print('\n')
                print("-------------- AGENDA -------------")
                print("SECCION PERSONAS ------------------")
                print("Presione 1 para crear persona")
                print("Presione 2 para guardar persona")
                print("Presione 3 para editar persona")
                print("Presione 4 para eliminar persona")
                print("Presione 5 para mostrar personas")
                print("SECCION NOTAS ---------------------")
                print("Presione 6 para crear nota")
                print("Presione 7 para guardar nota")
                print("Presione 8 para editar nota")
                print("Presione 9 para eliminar nota")
                print("Presione 10 para mostrar nota")
                print("-----------------------------------")
                print("Presione '<q>' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Seccion 1 - Creando informacion de persona")
                    self.crear_info_persona()
                elif seleccion == 2:
                    print("Seccion 2 - Guardando informacion de persona")
                    self.guardar_info_persona()
                elif seleccion == 3:
                    print("Seccion 3 - Editando informacion de persona")
                    self.editar_info_persona()
                elif seleccion == 4:
                    print("Seccion 4 - Eliminando informacion de persona")
                    self.eliminar_info_persona()
                elif seleccion == 5:
                    print("Seccion 5 - Mostrando informacion de personas")
                    self.mostrar_info_persona()
                elif seleccion == 6:
                    print("Seccion 6 - Creando informacion de nota")
                    self.crear_info_notas()
                elif seleccion == 7:
                    print("Seccion 7 - Guardando informacion de notas")
                    self.guardar_info_nota()
                elif seleccion == 8:
                    print("Seccion 8 - Editando informacion de nota")
                    self.editar_info_nota()
                elif seleccion == 9:
                    print("Seccion 9 - Eliminando informacion de nota")
                    self.eliminar_info_nota()
                elif seleccion == 10:
                    print("Seccion 10 - Mostrando informacion de nota")
                    self.mostrar_info_nota()
                else:
                    print("Ingrese una opción válida")

            except ValueError:
                print("Seleccione una opción válida")
            input("Presione <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("Agenda finalizada")
            sys.exit(0)
        return int(seleccion)

    def crear_info_persona(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero ingrese una persona")
            return False
        self.memoria.guardar_informacion_de_persona(self.persona)

    def editar_info_persona(self):
        self.memoria.editar_informacion_de_persona()

    def eliminar_info_persona(self):
        self.memoria.eliminar_informacion_de_persona()

    def mostrar_info_persona(self):
        self.memoria.mostrar_informacion_de_personas()

    def crear_info_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_nota(self):
        if not self.nota:
            print("Por favor, primero ingrese una nota")
            return False
        self.memoria.guardar_informacion_de_nota(self.nota)

    def editar_info_nota(self):
        self.memoria.editar_informacion_de_nota()

    def eliminar_info_nota(self):
        self.memoria.eliminar_informacion_de_nota()

    def mostrar_datos_nota(self):
        self.memoria.mostrar_informacion_de_nota()

    def mostrar_info_nota(self):
        self.memoria.mostrar_informacion_de_nota()


if __name__ == '__main__':
    menu = Menu()
    menu.run()
