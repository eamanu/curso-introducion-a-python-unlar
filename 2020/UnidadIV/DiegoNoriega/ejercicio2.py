import sys
import os


class Cola:
    def __init__(self):
        self.cola = []

    def agregar_elemento(self):
        x = input("Ingrese un elemento: ")
        self.cola.append(x)

    def quitar_elemento(self):
        if self.cola == []:
            print("Cola vacia, ya no puede quitar más elementos")
        else:
            print("El elemento quitado de la cola es: ", self.cola.pop(0))

    def tamano_cola(self):
        print('El tamaño de la cola es: ', len(self.cola))

    def vaciar_cola(self):
        if self.cola == []:
            print("No hay elementos para vaciar.")
        else:
            print("Vaciamos la cola:", self.cola.clear())

    def mostrar_estado_cola(self):
        if self.cola == []:
            print("Cola vacia. Ingrese elementos.")
        else:
            print("El estado de la cola es: ", self.cola)


class Menu:
    def __init__(self):
        self.cola = Cola()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'nt' else 'clear')
                print('\n')
                print("-------------- COLA -------------")
                print("Presione 1 para agregar nuevo elemento")
                print("Presione 2 para quitar elemento")
                print("Presione 3 para ver el tamaño de la cola")
                print("Presione 4 para vaciar la cola")
                print("Presione 5 para mostrar el estado de la cola")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    self.cola.agregar_elemento()
                elif seleccion == 2:
                    self.cola.quitar_elemento()
                elif seleccion == 3:
                    self.cola.tamano_cola()
                elif seleccion == 4:
                    self.cola.vaciar_cola()
                elif seleccion == 5:
                    self.cola.mostrar_estado_cola()
                else:
                    print("Presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("Cola finalizada")
            sys.exit(0)
        return int(seleccion)


if __name__ == '__main__':
    menu = Menu()
    menu.run()
