import sys
import os


class Pila:
    def __init__(self):
        self.pila = []

    def agregar_elemento(self):
        x = input("Ingrese un elemento: ")
        self.pila.append(x)

    def quitar_elemento(self):
        if self.pila == []:
            print("Pila vacia, ya no puede quitar más elementos")
        else:
            print("El elemento quitado de la pila es: ", self.pila.pop())

    def tamano_pila(self):
        print('El tamaño de la pila es: ', len(self.pila))

    def vaciar_pila(self):
        if self.pila == []:
            print("No hay elementos para vaciar.")
        else:
            print("Vaciamos la pila: ", self.pila.clear())

    def mostrar_estado_pila(self):
        if self.pila == []:
            print("Pila vacia. Ingrese elementos.")
        else:
            print("El estado de la pila es: ", self.pila)


class Menu:
    def __init__(self):
        self.pila = Pila()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'nt' else 'clear')
                print('\n')
                print("-------------- PILA -------------")
                print("Presione 1 para agregar elemento a la pila")
                print("Presione 2 para quitar elemento de la pila")
                print("Presione 3 para ver el tamaño de la pila")
                print("Presione 4 para vaciar la pila")
                print("Presione 5 para mostrar el estado de la pila")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    self.pila.agregar_elemento()
                elif seleccion == 2:
                    self.pila.quitar_elemento()
                elif seleccion == 3:
                    self.pila.tamano_pila()
                elif seleccion == 4:
                    self.pila.vaciar_pila()
                elif seleccion == 5:
                    self.pila.mostrar_estado_pila()
                else:
                    print("Presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("Pila finalizada")
            sys.exit(0)
        return int(seleccion)


if __name__ == '__main__':
    menu = Menu()
    menu.run()