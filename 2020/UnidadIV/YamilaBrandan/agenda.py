import sys
import os

class Persona:
    def __init__(self):
        self.nombre= None
        self.apellido=None
        self.telefono=None
        self.email=None
    def agregar_datos(self):  # importante
        # guardan en atributos
        self.nombre = input("Escribir nombre: ")
        self.apellido= input ("Escribir apellido: ")
        self.telefono= input ("Escribir teléfono: ")
        self.email= input ("Escribir email: ")


class Notas:
    def __init__(self):
        self.nombre_notas= None
        self.contenido=None

    def crear_notas(self): # importante
        self.colocar_nombre_notas()
        self.escribir_contenido_notas()


    def colocar_nombre_notas(self):
        self.nombre_notas = input("Ingrese el nombre de la nota")

    def escribir_contenido_notas(self):
        self.contenido = input("Agregar el contenido: ")


class Memoria:
    def __init__(self):
        pass
    def guardar_informacion_persona(self, persona): #importante
        with open ("personas.txt", "a") as f:
            f.write(persona.nombre)
            f.write('\n')
            f.write(persona.apellido)
            f.write('\n')
            f.write(persona.telefono)
            f.write('\n')
            f.write(persona.email)
            f.write('\n')
            f.write("-------------------------------------")
            f.write('\n')

    def guardar_informacion_notas(self, nota): #importante
        with open("notas.txt", 'a') as f:
            f.write(nota.nombre_notas)
            f.write('\n')
            f.write(nota.contenido)
            f.write('\n')
            f.write("-------------------------------------")
            f.write('\n')

    def mostrar_personas(self): # importantes
        with open("personas.txt", 'r') as f:
             print(f.read())

    def mostrar_notas(self): # importantes
        with open("notas.txt", "r") as f:
            print (f.read())


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name != 'posix' else 'clear')

                print("Presione 1 para crear y guardar nueva persona")
                print("Presione 2 para crear y guardar nueva nota")
                print("Presione 3 para mostrar todas las personas")
                print("Presione 4 para  mostrar todas las notas")

                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                    self.guardar_info_persona()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                    self.guardar_nota()
                elif seleccion == 3:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()

                elif seleccion == 4:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()


                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()
