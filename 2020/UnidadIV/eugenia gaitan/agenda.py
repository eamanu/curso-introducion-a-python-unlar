import sys
import os

class Persona:
    def __init__(self): 
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None
    def agregar_datos(self):  
        self.nombre = input("escribir nombre: ")
        self.apellido = input("escribir apellido: ")
        self.telefono = int(input("escribir telefono: "))
        self.email = input("escribir email: ")

class Notas:
    def __init__(self):  
        self.escribir_contenido_nota = None
        self.colocar_nombre_nota = None
    def crear_notas(self): 
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()
    def escribir_contenido_notas(self):
        self.escribir_contenido_nota = input("contenido: ")
    def colocar_nombre_notas(self):
        self.colocar_nombre_nota = input("nombre nota: ")
    

class Memoria:
    def __init__(self) : 
        pass
    def guardar_informacion_persona(self, persona): 
        print(persona.nombre)
        print(persona.apellido)
        print(persona.telefono)
        print(persona.email)
        with open("personas.txt", 'a') as f:
            f.write(persona.nombre)  
            f.write('\t')
            f.write(persona.apellido)
            f.write('\t')
            f.write(str(persona.telefono))
            f.write('\t')
            f.write(persona.email)
            f.write('\n')
    def guardar_informacion_notas(self, nota): 
        print(nota.colocar_nombre_nota)
        print(nota.escribir_contenido_nota)
        with open("notas.txt", 'a') as f:
            f.write(nota.colocar_nombre_nota)
            f.write('\t')
            f.write(nota.escribir_contenido_nota)
            f.write('\n')
    def mostrar_personas(self): 
        with open("personas.txt", 'r') as f:
            print(f.read())

    def mostrar_notas(self): 
        with open("notas.txt", 'r') as f:
            print(f.read())

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una nota")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()
