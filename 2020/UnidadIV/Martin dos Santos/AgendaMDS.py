import sys
import os

class Persona:
    
    def __init__(self):
        self.nombre=""
        self.apellido=""
        self.telefono=0
        self.email=""
        
    def agregar_datos(self):
        self.nombre = input("escribir Nombre: ")
        self.apellido =input("escribir Apellido: ")
        self.telefono =input("escribir Telefono: ")
        self.email = input ("escribir Email: ")
        pass

class Notas:
    def crear_notas(self):
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()
        pass
    def escribir_contenido_notas(self):
        self.contenido = input("contenido")
        pass
    def colocar_nombre_notas(self):
        self.nombre=input("Nombre nota: ")
        pass

class Memoria:
    
    def __init__(self):
        self.persona = None
        self.nota = None
        
    def guardar_informacion_persona(self, persona):
        with open("personas.txt",'a') as f:
            f.write(persona.nombre+"/")
            f.write(persona.apellido+"/")
            f.write(persona.telefono+"/")
            f.write(persona.email+"/")
            f.write('\n')            
        pass
    def guardar_informacion_notas(self, nota):
        with open("notas.txt",'a') as f:
            f.write(nota.nombre+"/")
            f.write(nota.contenido+"/")
            f.write('\n')
        pass
    def mostrar_personas(self):
        with open("personas.txt", 'r') as f:
             print(f.read())
        pass
    
    def eliminar_nota(self):
        with open("notas.txt",'r') as f:
            archivo=f.read().split('\n')
            c=int(len(archivo))-1
            if(c==0):
                print("Archivo vacio")
            else:
                for i in range(0,c):
                    print("indice nota= ",i," nota= ",archivo[i],"\n")
                ideliminar=int(input("Ingrese el id de la nota que quiere eliminar"))
                if(ideliminar>c):
                    print("Indice ingresado fuera de rango ",ideliminar," > ",c)
                else:
                    archivo.pop(ideliminar)
                    with open("notas.txt",'w') as d:
                        for i in range(0,c):
                            d.write(archivo[i])    
        pass
    
    def eliminar_persona(self):
        with open("personas.txt",'r') as f:
            archivo=f.read().split('\n')
            c=int(len(archivo))-1
            if(c==0):
                print("Archivo vacio")
            else:
                for i in range(0,c):
                    p=archivo[i].split("/")                    
                    print("Indice Persona: ",i)
                    print("Nombre: ",p[0])
                    print("Apellido: ",p[1])
                    print("Telefono: ",p[2])
                    print("Email: ",p[3])
                ideliminar=int(input("Ingrese el id de la persona que quiere eliminar: "))
                if(ideliminar>c):
                    print("Indice ingresado fuera de rango ",ideliminar," > ",c)
                else:
                    archivo.pop(ideliminar)
                    with open("personas.txt",'w') as d:
                        for i in range(0,c):
                            d.write(archivo[i])    
        pass
    
    def modificar_persona(self):
        with open("personas.txt",'r') as f:
            archivo=f.read().split('\n')
            c=int(len(archivo))-1
            if(c==0):
                print("Archivo vacio")
            else:
                for i in range(0,c):
                    p=archivo[i].split("/")                    
                    print("Indice Persona: ",i)
                    print(" Nombre: ",p[0])
                    print(" Apellido: ",p[1])
                    print(" Telefono: ",p[2])
                    print(" Email: ",p[3])
                idmodificar=int(input("Ingrese el id de la persona que quiere modificar: "))
                if(idmodificar>c):
                    print("Indice ingresado fuera de rango ",idmodificar," > ",c)
                else:
                    print("¿Que atributo desea modificar?")
                    print("1-> Nombre")
                    print("2-> Apellido")
                    print("3-> Telefono")
                    print("4-> Email")
                    print("q->salir")
                    opcion=input("-> ")
                    p=archivo[idmodificar].split("/")
                    if opcion == "1":
                        p[0]=input("Ingrese nuevo NOMBRE: ")                        
                    elif opcion == "2":
                        p[1]=input("Ingrese nuevo APELLIDO: ")
                    elif opcion == "3":
                        p[2]=input("Ingrese nuevo TELEFONO: ")
                    elif opcion == "4":
                        p[3]=input("Ingrese nuevo EMAIL: ")
                    else:
                        return
                    with open("personas.txt",'w') as d:
                        p='/'.join(p)
                        archivo[idmodificar]=p
                        for i in range(0,c):
                            d.write(archivo[i])   
                            
    def modificar_nota(self):
        with open("notas.txt",'r') as f:
            archivo=f.read().split('\n')
            c=int(len(archivo))-1
            if(c==0):
                print("Archivo vacio")
            else:
                for i in range(0,c):
                    p=archivo[i].split("/")                    
                    print("Indice Nota: ",i)
                    print(" Nombre Nota: ",p[0])
                    print(" Contenido: ",p[1])
                idmodificar=int(input("Ingrese el id de la persona que quiere modificar: "))
                if(idmodificar>c):
                    print("Indice ingresado fuera de rango ",idmodificar," > ",c)
                else:
                    print("¿Que atributo desea modificar?")
                    print("1-> Nombre de la nota")
                    print("2-> Contenido de la nota")
                    print("q->salir")
                    opcion=input("-> ")
                    p=archivo[idmodificar].split("/")
                    if opcion == "1":
                        p[0]=input("Ingrese nuevo NOMBRE: ")                        
                    elif opcion == "2":
                        p[1]=input("Ingrese nuevo CONTENIDO: ")
                    else:
                        return
                    with open("notas.txt",'w') as d:
                        p='/'.join(p)
                        archivo[idmodificar]=p
                        for i in range(0,c):
                            d.write(archivo[i])  
    def mostrar_notas(self): # importantes
        with open("notas.txt", 'r') as f:
             print(f.read())
        pass

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para modificar atributos de una persona")
                print("Presione 8 para modificar una nota")
                print("Presione 9 para eliminar una persona")
                print("Presione 10 para eliminar una nota")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Modificar atributos de una persona")
                    self.modificar_at_persona()
                elif seleccion == 8:
                    print("Modificar una nota")
                    self.modificar_nota()
                elif seleccion == 9:
                    print("Eliminar una persona")
                    self.eliminar_persona()
                elif seleccion == 10:
                    print("Eliminar una nota")
                    self.eliminar_nota()
                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()
    
    def eliminar_nota(self):
        self.memoria.eliminar_nota()
        
    def eliminar_persona(self):
        self.memoria.eliminar_persona()
    
    def modificar_at_persona(self):
        self.memoria.modificar_persona()
        
    def modificar_nota(self):
        self.memoria.modificar_nota()
        
    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()
