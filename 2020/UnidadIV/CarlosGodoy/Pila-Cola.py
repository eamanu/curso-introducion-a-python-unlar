class Pila:
    def __init__(self):
        self.pila = []
    def agrega_elemento (self, x):
        self.pila.append(x)
    def quitar_elemento (self):
        self.pila.pop()
    def tamaño_pila (self):
        print(len(self.pila))
    def vaciar_pila (self):
        self.pila.clear()
    def mostrar_estado (self):
        print (self.pila)

class Cola:
    def __init__(self):
        self.cola = []
    def agrega_elemento (self, x):
        self.cola.insert(0,x)
    def quitar_elemento (self):
        self.cola.pop()
    def tamaño_cola (self):
        print(len(self.cola))
    def vaciar_cola (self):
        self.cola.clear()
    def mostrar_estado (self):
        print (self.cola)

print("Ejemplo de Pila")
pila = Pila()
pila.mostrar_estado()
pila.agrega_elemento(10)
pila.agrega_elemento(11)
pila.agrega_elemento(12)
pila.agrega_elemento(13)
pila.agrega_elemento(14)
pila.agrega_elemento(15)
pila.tamaño_pila()
pila.mostrar_estado()
print("Quito Elemento...")
pila.quitar_elemento()
pila.mostrar_estado()
print("Quito Elemento...")
pila.quitar_elemento()
pila.mostrar_estado()
print("Quito Elemento...")
pila.quitar_elemento()
pila.mostrar_estado()
print("Vacio la pila")
pila.vaciar_pila()
pila.tamaño_pila()
pila.mostrar_estado()

print("Ejemplo de Cola")
cola = Cola()
cola.agrega_elemento(10)
cola.agrega_elemento(11)
cola.agrega_elemento(12)
cola.agrega_elemento(13)
cola.agrega_elemento(14)
cola.agrega_elemento(15)
print("Quito Elemento...")
cola.mostrar_estado()
cola.quitar_elemento()
print("Quito Elemento...")
cola.mostrar_estado()
cola.quitar_elemento()
print("Quito Elemento...")
cola.mostrar_estado()
cola.quitar_elemento()
print("Quito Elemento...")
cola.mostrar_estado()
print("Agrego Elementos...")
cola.agrega_elemento(16)
cola.agrega_elemento(17)
cola.agrega_elemento(18)
cola.agrega_elemento(19)
cola.agrega_elemento(20)
print("Quito Elemento...")
cola.quitar_elemento()
cola.mostrar_estado()
print("Quito Elemento...")
cola.quitar_elemento()
cola.mostrar_estado()
print("Vacio la Cola")
cola.vaciar_cola()
cola.tamaño_cola()
cola.mostrar_estado()