import sys
import os

class Persona:
    def __init__ (self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None
    def agregar_datos(self):
        self.nombre = input("Ingrese Nombre: ")
        self.apellido = input("Ingrese Apellido: ")
        self.telefono = input("Ingrese Telefono: ")
        self.email = input("Ingrese e-mail: ")

class Notas:
    def __init__ (self):
        self.nombre_nota = None
        self.contenido=None
    def crear_notas(self): # importante
        self.colocar_nombre_notas()
        self.escribir_contenido_notas()
    def escribir_contenido_notas(self):
        self.contenido=input("Ingrese Texto de Nota:")
    def colocar_nombre_notas(self):
        self.nombre_nota=input("Ingrese Nombre de Nota:")

class Memoria:
    def guardar_informacion_persona(self,persona):
        persona.nombre
        persona.apellido
        persona.telefono
        persona.email
        with open ("Personas.txt", 'a') as archivo:
            print("%s/%s/%s/%s" % (persona.nombre, persona.apellido, persona.telefono, persona.email),file=archivo)
            print("Guardando Personas en Archivo...")
            input("Presione <ENTER> para continuar")

    def guardar_informacion_notas(self, nota): #importante
        nota.nombre_nota
        nota.contenido
        with open ("Notas.txt", 'a') as archivo:
            print("%s/%s" % (nota.nombre_nota, nota.contenido),file=archivo)
            print("Guardando Notas en Archivo...")
            input("Presione <ENTER> para continuar")

    def mostrar_personas(self): # importantes
        with open ("Personas.txt", 'r') as archivo:
            print("Muestro Personas de Archivo...")
            print(archivo.read())
            input("Presione <ENTER> para continuar")

    def mostrar_notas(self): # importantes
        with open ("Notas.txt", 'r') as archivo:
            print("Muestro Notas de Archivo...")
            print(archivo.read())
            input("Presione <ENTER> para continuar")
            
    def modifico_persona(self):
        print("Modifica Persona")
        with open ("Personas.txt", 'r') as archivo:
            leo_todo = archivo.readlines()
        for indice, elemento in enumerate(leo_todo):
            print("Registro {}: {}".format(indice+1, elemento.strip("\n")))
        bandera = True
        while bandera is True:
            elijo_registro = input("Ingrese Nro de registro: ")
            elijo_registro = int (elijo_registro)
            if elijo_registro <= 0 or elijo_registro > indice+1:
                    print("Registro Fuera de Rango... Verifique valor ingresado")
                    bandera = True
            else:
                bandera = False
        registro_leido=leo_todo[elijo_registro-1]
        registro_leido = registro_leido.split('/') # Separo en elementos de lista, el registro elegido por el usuario.
        print("Registro Elegido")
        print("Nombre: %s / Apellido:%s / Telefono:%s / E-Mail:%s"% (registro_leido[0], registro_leido[1], registro_leido[2], registro_leido[3]))
        print ("Modifico Datos")
        bandera = True
        while bandera is True:
            print("Ingrese Opcion para modificar un dato")
            print("1-Nombre")
            print("2-Apellido")
            print("3-Telefono")
            print("4-E_Mail")
            print("9-No modifica")
            seleccion = input("Seleccione una opción ")
            seleccion = int (seleccion)
            if seleccion == 1:
                print("Nombre: %s"% (registro_leido[0]))
                registro_leido[0]=input("Ingrese nuevo nombre: ") # Guardo cambio realizado por usuario en elemento de lista
                bandera = False
            elif seleccion == 2:
                print("Apellido: %s"% (registro_leido[1]))
                registro_leido[1]=input("Ingrese nuevo apellido: ")
                bandera = False
            elif seleccion == 3:
                print("Telefono: %s"% (registro_leido[2]))
                registro_leido[2]=input("Ingrese nuevo telefono: ")
                bandera = False
            elif seleccion == 4:
                print("E-mail: %s"% (registro_leido[3]))
                registro_leido[3]=input("Ingrese nuevo E-Mail: ")
                bandera = False
            elif seleccion == 9:
                input("Modificaciones NO GUARDADAS / Presione <ENTER> para continuar")
                menu.run()
            else:
                print("presione una opción válida")
                bandera = True
        leo_todo[elijo_registro-1] = registro_leido[0] + "/" + registro_leido[1] + "/" + registro_leido[2] + "/" + registro_leido[3] # Reemplazo String con dato modificado por usuario
        with open ("Personas.txt", 'w') as archivo:
            for elemento in leo_todo:
                archivo.write("{}".format(elemento)) # Escribo archivo con nuevos valores, sin perder los elementos existentes
        input("Modificaciones GUARDADAS / Presione <ENTER> para continuar")

    def borro_persona (self):
        print("Borra Persona")
        with open ("Personas.txt", 'r') as archivo:
            leo_todo = archivo.readlines()
        for indice, elemento in enumerate(leo_todo):
            print("Registro {}: {}".format(indice+1, elemento.strip("\n")))
        elijo_registro = input("Ingrese Nro de registro: ")
        elijo_registro = int (elijo_registro)
        leo_todo.pop((elijo_registro-1)) # Elimino Persona elegido por usuario
        with open ("Personas.txt", 'w') as archivo:
            for elemento in leo_todo:
                archivo.write("{}".format(elemento)) # Escribo archivo con lista actualizada
        input("Persona BORRADA / Presione <ENTER> para continuar")

    def modifico_nota(self):
        print("Modifica Notas")
        with open ("Notas.txt", 'r') as archivo:
            leo_todo = archivo.readlines()
        for indice, elemento in enumerate(leo_todo):
            print("Registro {}: {}".format(indice+1, elemento.strip("\n")))
        bandera = True
        while bandera is True:
            elijo_registro = input("Ingrese Nro de registro: ")
            elijo_registro = int (elijo_registro)
            if elijo_registro <= 0 or elijo_registro > indice+1:
                    print("Registro Fuera de Rango... Verifique valor ingresado")
                    bandera = True
            else:
                bandera = False
        registro_leido = leo_todo[elijo_registro-1]
        registro_leido = registro_leido.split('/')
        print("Nota Elegida")
        print("Nombre: %s / Contenido:%s"% (registro_leido[0], registro_leido[1]))
        print ("Modifico Datos")
        bandera = True
        while bandera is True:
            print("Ingrese Opcion para modificar un dato")
            print("1 - Nombre Nota")
            print("2 - Contenido Nota")
            print("9 - No modifica")
            seleccion = input("Seleccione una opción ")
            seleccion = int (seleccion)
            if seleccion == 1:
                print("Nombre Nota: %s"% (registro_leido[0]))
                registro_leido[0]=input("Ingrese nuevo nombre de nota: ")
                bandera = False
            elif seleccion == 2:
                print("Contenido Nota: %s"% (registro_leido[1]))
                registro_leido[1]=input("Ingrese nuevo contenido: ")
                bandera = False
            elif seleccion == 9:
                input("Modificaciones NO GUARDADAS / Presione <ENTER> para continuar")
                menu.run()
            else:
                print("Presione una opción válida")
                bandera = True 
        leo_todo[elijo_registro-1] = registro_leido[0] + "/" + registro_leido[1]
        print("Info Leo_todo")
        print(id(leo_todo))
        print(leo_todo)
        print("Info Leo_todo [elijo_registro-1]")
        print(id(leo_todo[elijo_registro-1]))
        print(leo_todo[elijo_registro-1])
        input("Verifico....")
        with open ("Notas.txt", 'w') as archivo:
            for elemento in leo_todo:
                archivo.write("{}".format(elemento))
        input("Modificaciones GUARDADAS / Presione <ENTER> para continuar")

    def borro_nota(self):
        print("Borra Nota")
        with open ("Notas.txt", 'r') as archivo:
            leo_todo = archivo.readlines()
        for indice, elemento in enumerate(leo_todo):
            print("Registro {}: {}".format(indice+1, elemento.strip("\n")))
        elijo_registro = input("Ingrese Nro de Nota: ")
        elijo_registro = int (elijo_registro)
        leo_todo.pop((elijo_registro-1))
        with open ("Notas.txt", 'w') as archivo:
            for elemento in leo_todo:
                archivo.write("{}".format(elemento))
        input("Nota BORRADA / Presione <ENTER> para continuar")

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
             try:
                os.system('cls' if os.name == 'nt' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("7 - Modifico Persona")
                print("8 - Borro Persona")
                print("9 - Modifico Nota")
                print("10 - Borro Nota")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Modifico Persona...")
                    self.modifico_persona()
                elif seleccion == 8:
                    print("Borro Persona...")
                    self.borro_persona()
                elif seleccion == 9:
                    print("Modifico Nota...")
                    self.modifico_nota()
                elif seleccion == 10:
                    print("Borro Nota...")
                    self.borro_nota()
                else:
                    print("presione una opción válida")
             except ValueError:
                      print("Seleccione una opción válida")
        input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()

    def modifico_persona(self):
        self.memoria.modifico_persona()

    def borro_persona(self):
        self.memoria.borro_persona()

    def modifico_nota(self):
        self.memoria.modifico_nota()

    def borro_nota(self):
        self.memoria.borro_nota()

if __name__ == '__main__':
    menu = Menu()
    menu.run()