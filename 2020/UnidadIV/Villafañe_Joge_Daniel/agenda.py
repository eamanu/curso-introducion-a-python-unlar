import sys
import os


class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None

    def agregar_datos(self):
        self.nombre = "Nombre:\t" + (input("Ingrese Nombre: "))
        self.apellido = "Apellido:\t" + input("Ingrese Apellido: ")
        self.telefono = "Telefono:\t" + input("Ingrese teléfono: ")
        self.email = "Email:\t" + input("Ingrese email: ")


class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.nota = None

    def crear_notas(self):
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()

    def escribir_contenido_notas(self):
        self.nota = input("Escriba la nota: ")

    def colocar_nombre_notas(self):
        self.nombre_nota = input("Escriba el nombre de la nota: ")


class Memoria:
    def guardar_informacion_persona(self, persona):
        with open("persona.txt", "a") as f:
            print(persona.nombre, persona.apellido, persona.telefono, persona.email, "----------------------", file=f,
                  sep='\n')

    def mostrar_personas(self):
        with open("persona.txt", "r") as f:
            print(f.read())

    def guardar_informacion_notas(self, nota):
        with open("notas.txt", "a") as f:
            print(nota.nombre_nota+".txt", file=f, sep='\n')
            with open(nota.nombre_nota+".txt", "w") as f2:
                print("Titulo: "+nota.nombre_nota, "nota: "+nota.nota, file=f2, sep='\n')

    def mostrar_notas(self):
        with open("notas.txt", "r") as f:
            for linea in f.readlines():
                with open(linea.replace('\n', ''), "r") as f2:
                    print(f2.read())


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()


if __name__ == '__main__':
    menu = Menu()
    menu.run()
