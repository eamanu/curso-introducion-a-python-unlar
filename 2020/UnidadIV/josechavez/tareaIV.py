import sys
import os


class Persona:
    def __init__(self, nombre = None, apellido = None, telefono = None, email = None):

        pass

    def agregar_datos(self):
        # guardan en atributos
        self.nombre = input("Ingrese Nombre: ")
        self.apellido = input("Ingrese Apellido: ")
        self.telefono = input("Ingrese Teléfono: ")
        self.email = input("Ingrese Email: ")
        pass

    def __str__(self):
        return "nombre" + "apellido" + "telefono" + "email"

        pass

class Notas:

    def __init__(self, nombre_nota = None, nota = None):

        pass

    def crear_notas(self):  # importante
        self.colocar_nombre_notas()
        self.escribir_contenido_notas()

        pass

    def colocar_nombre_notas(self):
        self.nombre_nota = input("Ingrese nombre de la nota: ")

        pass

    def escribir_contenido_notas(self):
        self.nota = input("Ingrese contenido de la nota: ")
        # nombre de la nota
        pass


class Memoria:
    # init definir los atributos

    def guardar_informacion_persona(self, persona):  # importante

        with open("personas.txt", 'a') as f:
            f.write(persona.nombre + '\t')
            f.write(persona.apellido + '\t')
            f.write(persona.telefono + '\t')
            f.write(persona.email + '\t')
            f.write("\n")

        pass

    def guardar_informacion_notas(self, nota):  # importante

        with open("notas.txt", 'a') as f:
            f.write(nota.nombre_nota + '\t')
            f.write(nota.nota + '\t')
            f.write("\n")
        pass

    def mostrar_personas(self):  # importantes
        with open("personas.txt", 'r') as f:
            personas = f.read()
            print("Nombre\t"+"Apellido\t"+"Telefono\t"+"Email")
            print(personas)
        pass

    def mostrar_notas(self):  # importantes
        with open("notas.txt", 'r') as f:
            notas = f.read()
            print("Nota\t"+"Contenido\t")
            print(notas)
        pass


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('clear' if os.name == 'net' else 'cls')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Ingrese los datos de la nueva persona:")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Ingrese los datos de la nueva nota:")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("La persona se grabó exitosamente")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("La nota se grabó exitosamente")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas")
                    self.mostrar_notas()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()


if __name__ == '__main__':
    menu = Menu()
    menu.run()