#PILA Y COLA:

class Pila:
    def __init__(self):
        self.pila = []
        pass
    def agregar_elemento(self):
        self.pila.append(input("Ingresar elemento nuevo:"))
    def quitar_elemento(self):
        if len(self.pila) == 0:
            print("La pila ya esta vacia")
        else:
            self.pila.pop()
    def tamano_pila(self):
        print(len(self.pila))
    def vaciar_pila(self):
        self.pila.clear()
    def mostar_estado_pila(self):
        print(self.pila)

class Cola:
    def __init__(self):
        self.cola = []
        pass
    def agregar_elemento(self):
        self.cola.append("Agregar elemento a la cola")
    def quitar_elemento(self):
        self.cola.pop(0)
    def tamaño_cola(self):
        print(len(self.cola))
    def mostrar_estado_cola(self):
        print(self.cola)
    def vaciar_cola(self):
        self.pila.clear()



#AGENDA


import sys
import os
import json

class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.email = None
        self.telefono = None
        pass
    def agregar_datos_persona(self):
        self.nombre = input("Ingrese el nombre: ")
        self.apellido = input("Ingrese el apellido: ")
        self.email = input("Ingrese el email: ")
        self.telefono = int(input("Ingrese el numero de telefono: "))
        pass


class Menu:
    def __init__(self):
        self.persona = None
        self.d_persona = {}
        self.d_agenda = {"lista_personas": []}


    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona a la agenda")
                print("Presione 2 para mostrar todas las persona")
                print("Presione 3 para borrar una persona")
                print("Presione 4 para editar una persona")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_persona()
                elif seleccion == 2:
                    print("Mostrando la agenda almacenada en el archivo:")
                    self.mostrar_agenda()
                elif seleccion == 3:
                    print("A continuacion se eliminara una persona:")
                    self.borrar_persona()
                elif seleccion == 4:
                    print("A continuacion se editara una persona:")
                    self.editar_persona()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    def agregar_persona(self):
        self.persona = Persona()
        self.persona.agregar_datos_persona()
        self.d_persona = {"nombre": self.persona.nombre, "apellido": self.persona.apellido,
                          "email": self.persona.email, "telefono": self.persona.telefono}
        self.d_agenda['lista_personas'].append(self.d_persona)
        with open("personas.json", 'w') as f:
            f.write(json.dumps(self.d_agenda))

    def mostrar_agenda(self):
        with open("personas.json", 'r') as f:
            contenido = f.read()
            self.d_agenda = json.loads(contenido)
        for p in self.d_agenda["lista_personas"]:
            print(p)

    def borrar_persona(self):
        with open("personas.json", 'r') as f:
            contenido = f.read()
            self.d_agenda = json.loads(contenido)

        nombre = input("Ingrese el nombre de la persona que desea borrar: ")

        for p in range(len(self.d_agenda["lista_personas"])):

            if self.d_agenda["lista_personas"][p]["nombre"] == nombre:
                self.d_agenda["lista_personas"].pop(p)
                print(self.d_agenda["lista_personas"])

                with open("personas.json", 'w') as f:
                    f.write(json.dumps(self.d_agenda))
                break

            else:
                print("No existe una persona con ese nombre")


    def editar_persona(self):
        with open("personas.json", 'r') as f:
            contenido = f.read()
            self.d_agenda = json.loads(contenido)

        nombre = input("Ingrese el nombre de la persona que desea editar: ")

        for p in range(len(self.d_agenda["lista_personas"])):
            if self.d_agenda["lista_personas"][p]["nombre"] == nombre:
                nom = input("Ingrese el nuevo nombre: ")
                ape = input("Ingrese el nuevo apellido: ")
                email = input("Ingrese el nuevo email: ")
                telefono = input("Ingrese el nuevo telefono: ")
                self.d_agenda["lista_personas"][p]["nombre"] = nom
                self.d_agenda["lista_personas"][p]["apellido"] = ape
                self.d_agenda["lista_personas"][p]["email"] = email
                self.d_agenda["lista_personas"][p]["telefono"] = telefono
                with open("personas.json", 'w') as f:
                    f.write(json.dumps(self.d_agenda))
                break
            else:
                print("No existe una persona con ese nombre")


if __name__ == '__main__':
    menu = Menu()
    menu.run()

