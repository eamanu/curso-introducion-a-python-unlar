import sys
import os
import json

class TAGS:
    def __init__(self):
        self.NOMBRE = "NOMBRE"
        self.APELLIDO = "APELLIDO"
        self.TELEFONO = "TELEFONO"
        self.EMAIL = "EMAIL"
        self.NOTA_NOMBRE ="NOMBRE"
        self.NOTA_CONTENIDO="CONTENIDO"
        self.archivo_persona='personas.json'
        self.archivo_notas='notas.json'
        self.archivo_keys_personas = 'listado_keys_personas.json'
        self.archivo_keys_notas = 'listado_keys_notas.json'

class Persona:
    
    def __init__(self):
        self.tags=TAGS()
        self.datos={}
        
    def verificar_archivo(self):
        print("verif_arch")
        if not(os.path.exists(self.tags.archivo_persona)):
            with open(self.tags.archivo_persona,'w') as f:
                f.write(json.dumps({}))
                print("Nuevo archivo personas.json creado")
            with open(self.tags.archivo_keys_personas,'w') as f:
                f.write(json.dumps({"0":["-1"]}))
        else:
            print("El archivo personas.json existe")
            pass
    
    def verificar_key(self,key):
        listaAux=[]
        a_file=open(self.tags.archivo_keys_personas, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        listaAux=b["0"]
        
        if(len(listaAux)> 0 and listaAux[0] != (-1) ):
            while str(key) in listaAux:
                key+=1
            
            listaAux.append(str(key))
        else:
            listaAux[0]=(str(key))
         
        listaAux.sort()
        b["0"]= listaAux    
        
        a_file=open(self.tags.archivo_keys_personas, 'w')        
        json.dump(b,a_file)
        a_file.close()
        
        return key
            
                
                
    
    def get_id(self):
        with open(self.tags.archivo_persona,'r') as archivo:
            a=archivo.read()
            a=json.loads(a)
            return (len(a))
            
            
    
    def agregar_datos(self):
        self.verificar_archivo()    #verifica la existencia del archivo y si no, crea uno vacio
        cantidadPersonas = self.verificar_key(self.get_id() + 1)
        self.datos={str(cantidadPersonas) :{self.tags.NOMBRE : input("Escribir Nombre: "),
                                            self.tags.APELLIDO : input("Escribir Apellido: "),
                                            self.tags.TELEFONO : input("Escribir Telefono: "),
                                            self.tags.EMAIL : input("Escribir Email: ")}}
                    
    
    
            

class Notas:
    def __init__(self):
        self.nueva_nota={}
        self.tags= TAGS()
        
    def verificar_archivo(self):
        
        if not(os.path.exists(self.tags.archivo_notas)):
            with open(self.tags.archivo_notas,'w') as f:
                f.write(json.dumps({}))
            with open(self.tags.archivo_keys_notas,'w') as f:
                f.write(json.dumps({"0":["-1"]}))
                print("Nuevo archivo notas.json creado")
        else:
            print("El archivo notas.json existe")
            pass
    
    def verificar_key(self,key):
        listaAux=[]
        a_file=open(self.tags.archivo_keys_notas, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        listaAux=b["0"]
        
        if(len(listaAux)> 0 and listaAux[0] != (-1) ):
            while str(key) in listaAux:
                key+=1
            
            listaAux.append(str(key))
        else:
            listaAux[0]=(str(key))
            
        listaAux.sort()          
        b["0"]= listaAux
        
        a_file=open(self.tags.archivo_keys_notas, 'w')        
        json.dump(b,a_file)
        a_file.close()
        
        return key
    
    
    def get_id(self):
        with open(self.tags.archivo_notas,'r') as archivo:
            a=archivo.read()
            a=json.loads(a)
            return (len(a))
    
    def crear_notas(self):
        self.verificar_archivo()
        cantidad_notas = self.verificar_key(self.get_id() + 1)
        self.nueva_nota={str(cantidad_notas):{self.tags.NOTA_NOMBRE : input("Escribir un nombre para la nota: "),
                                              self.tags.NOTA_CONTENIDO : input("Escribir el contenido de la nota: ")
                                                }}
        pass
    

class Memoria:
    
    def __init__(self):
        self.persona = None
        self.nota = None
        self.tags=TAGS()
        
    def guardar_informacion_persona(self, persona):
        a_file=open(self.tags.archivo_persona, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        
        key=str(persona.get_id()+1)
        b[key]=persona.datos[key]
        a_file=open(self.tags.archivo_persona, 'w')        
        json.dump(b,a_file)
        a_file.close()
            
            
    def guardar_informacion_notas(self, nota):
        a_file=open(self.tags.archivo_notas, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        
        key=str(nota.get_id()+1)
        b[key]=nota.nueva_nota[key]
        a_file=open(self.tags.archivo_notas, 'w')        
        json.dump(b,a_file)
        a_file.close()
        
        
        pass
    
    def modificar_persona(self):
        a_file=open(self.tags.archivo_persona, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        
        id_modificar=input("Ingrese el ID de la persona que desea modificar: ")
        
        print("¿Qué atributos desea modificar?")
        print("1) Nombre")
        print("2) Apellido")
        print("3) Telefono")
        print("4) Email")
        print("q) Volver al menu principal")
        opcion=input("-> ")
        if(opcion == "1"):
            b[str(id_modificar)][self.tags.NOMBRE]=input("Ingrese nuevo Nombre: ")
        elif(opcion == "2"):
            b[str(id_modificar)][self.tags.APELLIDO]=input("Ingrese nuevo Apellido: ")
        elif(opcion == "3"):
            b[str(id_modificar)][self.tags.TELEFONO]=input("Ingrese nuevo Telefono: ")
        elif(opcion == "4"):
            b[str(id_modificar)][self.tags.EMAIL]=input("Ingrese nuevo Email: ")
        
        if(opcion!= "q"):
            a_file=open(self.tags.archivo_persona, 'w')        
            json.dump(b,a_file)
            a_file.close()
        else:
            return
                            
    def modificar_nota(self):
        a_file=open(self.tags.archivo_notas, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        id_modificar=input("Ingrese el ID de la nota que desea modificar: ")
             
        print("¿Que atributo desea modificar?")
        print("1-> Nombre de la nota")
        print("2-> Contenido de la nota")
        print("q->salir")    
        opcion=input("-> ")
        if opcion == "1":
                        b[str(id_modificar)][self.tags.NOTA_NOMBRE]=input("Ingrese nuevo NOMBRE: ")                        
        elif opcion == "2":
                        b[str(id_modificar)][self.tags.NOTA_CONTENIDO]=input("Ingrese nuevo CONTENIDO: ")                        
       
        if(opcion!= "q"):
            a_file=open(self.tags.archivo_notas, 'w')        
            json.dump(b,a_file)
            a_file.close()
        else:
            return
        
        
    def mostrar_notas(self):
        try:
            with open(self.tags.archivo_notas) as f:
                archivo2=f.read()
                archivo2=json.loads(archivo2)
                for i in archivo2.keys():
                    print("ID: ", i)
                    print("Nombre de la nota: ",archivo2[i][self.tags.NOTA_NOMBRE])
                    print("Contenido de la nota: ",archivo2[i][self.tags.NOTA_CONTENIDO])
                
        except ValueError:
            print("No existe ningun archivo Notas")
        pass
    
    def mostrar_personas(self):
        try:
            with open(self.tags.archivo_persona) as f:
                archivo2=f.read()
                archivo2=json.loads(archivo2)
                for i in archivo2.keys():
                    print("ID: ", i)
                    print("Nombre: ",archivo2[i][self.tags.NOMBRE])
                    print("Apellido: ",archivo2[i][self.tags.APELLIDO])
                    print("Telefono: ",archivo2[i][self.tags.TELEFONO])
                    print("Email: ",archivo2[i][self.tags.EMAIL])
                    
        except ValueError:
            print("No existe ningun archivo Persona")
        pass
    
    def eliminar_nota(self):
        a_file=open(self.tags.archivo_notas, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        
        eliminar_id=input("Ingrese el ID de la nota que desea eliminar: ")
        del b[str(eliminar_id)]
        self.borrar_key(eliminar_id,self.tags.archivo_keys_notas)
        
        a_file=open(self.tags.archivo_notas, 'w')        
        json.dump(b,a_file)
        a_file.close()
        
        pass
    
    def eliminar_persona(self):
        a_file=open(self.tags.archivo_persona, 'r')
        b=json.loads(a_file.read())
        a_file.close()
        
        eliminar_id=input("Ingrese el ID de la persona que desea eliminar: ")
        del b[str(eliminar_id)]
        self.borrar_key(eliminar_id,self.tags.archivo_keys_personas)
        
        
        a_file=open(self.tags.archivo_persona, 'w')        
        json.dump(b,a_file)
        a_file.close()
    
    def borrar_key(self,key,nombreArchivo):
        a_file=open(nombreArchivo, 'r')
        c=json.loads(a_file.read())
        a_file.close()
        
        c["0"].remove(str(key))
        a_file=open(nombreArchivo, 'w')        
        json.dump(c,a_file)
        a_file.close()
    
class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para modificar atributos de una persona")
                print("Presione 8 para modificar una nota")
                print("Presione 9 para eliminar una persona")
                print("Presione 10 para eliminar una nota")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Modificar atributos de una persona")
                    self.modificar_at_persona()
                elif seleccion == 8:
                    print("Modificar una nota")
                    self.modificar_nota()
                elif seleccion == 9:
                    print("Eliminar una persona")
                    self.eliminar_persona()
                elif seleccion == 10:
                    print("Eliminar una nota")
                    self.eliminar_nota()
                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()
    
    def eliminar_nota(self):
        self.memoria.eliminar_nota()
        
    def eliminar_persona(self):
        self.memoria.eliminar_persona()
    
    def modificar_at_persona(self):
        self.memoria.modificar_persona()
        
    def modificar_nota(self):
        self.memoria.modificar_nota()
        
    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()