class Pila: #LIFO
    def __init__(self):
        self.pila = [ ]
    def agregar_elemento(self, x):
        self.pila.append(x)
    def quitar_elemento(self):
        self.pila.pop()
    def tamaño_pila(self):
        print(len(self.pila))
    def vaciar_pila(self):
        self.pila.clear()
    def mostrar_estado_pila(self):
        print(self.pila)
pila = Pila()
