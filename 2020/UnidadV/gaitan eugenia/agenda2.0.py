import json
import sys
import os


class Persona:
    def __init__(self):
        pass
    def agregar_datos(self):
        self.nombre = input("Ingrese un Nombre: ")
        self.apellido = input("Ingrese un Apellido: ")
        self.telefono = input("Ingrese un telefono: ")
        self.correo = input("Ingrese un correo: ")
        self.edad = input("Ingrese su edad: ")
        self.base_datos = {}

    def cargar_diccionario_personas(self):
        try:
            with open('persona.json', 'r') as f:
                self.base_datos = json.loads(f.read())
        except FileNotFoundError:
            self.base_datos = {"lista_persona": []}

    def editar_persona(self):
        self.cargar_diccionario_personas()
        for persona in self.base_datos['lista_persona']:
            if persona['nombre'] == self.nombre:
                persona['apellido'] = input("Apellido: ")
                persona['telefono'] = input("Telefono: ")
                persona['correo'] = input("Correo: ")
                persona['edad'] = input("Edad: ")
                # ...
        with open('persona.json', 'w') as f:
            f.write(json.dumps(self.base_datos))

    def mostrar_datos_personas(self):
        self.cargar_diccionario_personas()
        for persona in self.base_datos['lista_persona']:
            print("\nnombre: ", persona['nombre'])
            print("apellido: ", persona['apellido'])
            print("telefono: ", persona['telefono'])
            print("correo: ", persona['correo'])
            print("edad: ", persona['edad'])

    def guardar_info(self):
        self.cargar_diccionario_personas()
        p = {"nombre": self.nombre,"apellido": self.apellido,"telefono":self.telefono,"correo":self.correo,"edad":self.edad}
        self.base_datos['lista_persona'].append(p)

        with open('persona.json', 'w') as f:
            f.write(json.dumps(self.base_datos, indent=4))


class Menu:
    def __init__(self):
        self.persona = None

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para guardar información de persona")
                print("Presione 3 para mostrar todas las persona")
                print("Presione 4 para editar la persona")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    persona.agregar_datos()
                elif seleccion == 2:
                    print("Guardar info de persona...")
                    persona.guardar_info()
                    persona.mostrar_datos_personas()
                elif seleccion == 3:
                    print("Mostrar datos de todas las personas...", "\n")
                    persona.mostrar_datos_personas()
                elif seleccion ==4:
                    print("Editando persona")
                    persona.editar_persona()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("Hasta la proximaa!!")
            sys.exit(0)
        return int(seleccion)

persona = Persona()

if __name__ == '__main__':
    menu = Menu()
    menu.run()

