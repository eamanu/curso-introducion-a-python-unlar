from collections import deque

class Cola: #FIFO
    def __init__(self):
        self.cola = deque([ ])
    def agregar_elemento(self, x):
        self.cola.append(x)
    def quitar_elemento(self):
        self.cola.popleft()
    def tamaño_cola(self):
        print(len(self.cola))
    def vaciar_cola(self):
        self.cola.clear()
    def mostrar_estado_cola(self):
        print(self.cola)
cola = Cola()
