import sys
import os
import json

class Persona:
    def __init__(self):
        self.nombre=None
        self.apellido=None
        self.telefono=None
        self.email=None
        self.diccio=None
    def agregar_datos(self):
        self.diccio={}
        respuesta=""
        while respuesta!="no":
            self.nombre=input("Escribir nombre: ")
            self.apellido=input("Escribir apellido: ")
            self.telefono=input("Escribir teléfono: ")
            self.email=input("Escribir email: ")
            key=str(self.nombre+" "+self.apellido)
            d_persona={}
            d_persona["Nombre"]=str(self.nombre)
            d_persona["Apellido"]=str(self.apellido)
            d_persona["Telefono"]=str(self.telefono)
            d_persona["Email"]=str(self.email)
            self.diccio[key]=d_persona
            print(d_persona)
            print("Persona creada")
            respuesta=str(input("¿Desea crear otra persona?:"))

class Notas:
    def __init__(self):
        self.nombre_nota=None
        self.nota=None
        self.diccio={}
    def crear_notas(self):
        respuesta=""
        while respuesta !="no":
            self.colocar_nombre_nota()
            self.escribir_contenido_nota()
            key=str(self.nombre_nota)
            d_nota={}
            d_nota["Nombre"]=str(self.nombre_nota)
            d_nota["Contenido"]=str(self.nota)
            self.diccio[key]=d_nota
            print(d_nota)
            print("Nota creada")
            respuesta=str(input("¿Desea crear otra nota?: "))
    def colocar_nombre_nota(self):
        self.nombre_nota=(input("Escribir título de la nota: "))
    def escribir_contenido_nota(self):
        self.nota=(input("Escribir contenido de la nota: "))

class Memoria:
    def guardar_informacion_persona(self, Persona):
        with open("C:\\Users\\cielo\\Desktop\\Tarea\\personas.json","w") as f:
            f.write(json.dumps(Persona.diccio))
            print("¡Persona guardada!")
    def guardar_informacion_notas(self,Notas):
        with open("C:\\Users\\cielo\\Desktop\\Tarea\\notas.json", 'w') as f:
            f.write(json.dumps(Notas.diccio))
            print("¡Nota guardada!")
    def mostrar_personas(self):
        with open("C:\\Users\\cielo\\Desktop\\Tarea\\personas.json","r") as f:
            for i in Persona.diccio[key][d_persona]:
                print("Nombre: "+ d_persona[Nombre])
                print("Apellido: " + d_persona[Apellido])
                print("Teléfono: " + d_persona[Telefono])
                print("Email: " + d_persona[Email])
    def mostrar_notas(self):
        with open("C:\\Users\\cielo\\Desktop\\Tarea\\notas.json","r") as f:
            for i in Notas.diccio[key][d_nota]:
                print("Nombre: " + d_nota[Nombre])
                print("Contenido: " + d_nota[Contenido])
    def editar_persona(self):
        print("Editando persona...")
        diccio2={}
        respuesta=""
        while respuesta!="no":
            dp={}
            objeto=str(input("Ingresar nombre y apellido de la persona a editar: "))
            Persona.nombre=input("Escribir nombre: ")
            Persona.apellido=input("Escribir apellido:")
            Persona.telefono=input("Escribir teléfono:")
            Persona.email=input("Escribir email:")
            dp["Nombre"]=str(Persona.nombre)
            dp["Apellido"]=str(Persona.apellido)
            dp["Telefono"]=str(Persona.telefono)
            dp["Email"]=str(Persona.email)
            diccio2[objeto]=dp
            Persona.diccio.update(diccio2)
            print(dp)
            respuesta=str(input("¿Desea crear otra persona?:"))
        print("¡Se ha cambiado los datos de la persona!")
    def borrar_persona(self):
        print("Borrando persona...")
        del Persona.diccio[str(input("Ingresar nombre y apellido de la persona a borrar: "))]
        print("¡Persona borrada!")
    def editar_nota(self):
        print("Editando nota...")
        diccio2={}
        respuesta=""
        while respuesta !="no":
            dn={}
            objeto=str(input("Ingresar nombre de la nota a editar: "))
            Notas.nombre_nota=input("Escribir nombre de nota: ")
            Notas.nota=input("Escribir contenido de nota")
            dn["Nombre"]=str(Notas.nombre_nota)
            dn["Contenido"]=str(Notas.nota)
            diccio2[objeto]=dn
            Notas.diccio.update(diccio2)
            print(dn)
            respuesta=str(input("¿Desea crear otra nota?: "))
        print("¡Nota borrada!")
    def borrar_nota(self):
        print("Borrando nota...")
        del Notas.diccio[str(input("Ingresar nombre de la nota a borrar: "))]
        print("¡Nota borrada!")

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()
    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')
                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las personas")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para editar una persona")
                print("Presione 8 para editar una nota")
                print("Presione 9 para borrar una persona")
                print("Presione 10 para borrar una nota")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion ==7:
                    print("Editar persona...")
                    self.editar_personas()
                elif seleccion==8:
                    print("Editar nota...")
                    self.editar_notas()
                elif seleccion==9:
                    print("Borrar persona...")
                    self.borrar_personas()
                elif seleccion==10:
                    print("Borrar nota...")
                    self.borrar_notas()
                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()

    def editar_personas(self):
        self.memoria.editar_persona()

    def editar_notas(self):
        self.memoria.editar_nota()

    def borrar_personas(self):
        self.memoria.borrar_persona()

    def borrar_notas(self):
        self.memoria.borrar_nota()

if __name__ == '__main__':
    menu = Menu()
    menu.run()

for i in lista_persona:
    if p.nombre==rut:
        print