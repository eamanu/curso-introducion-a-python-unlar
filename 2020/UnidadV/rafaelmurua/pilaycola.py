class Pila: 
    def  __init__(self): 
        self.pila = [] 
    def agregar_elemento(self, x): 
        self.pila.append(x) 
    def quitar_elemento(self): 
        self.pila.pop() 
    def tamano_pila(self): 
        print(len(self.pila)) 
    def vaciar_pila(self): 
        self.pila.clear() 
    def mostrar_estado_pila(self): 
        print(self.pila) 
  
  

class Cola: 
    def  __init__(self): 
        self.cola = [] 
    def agregar_elemento(self, x): 
        self.cola.append(x) 
    def quitar_elemento(self): 
        self.cola.pop(0) 
    def tamano_cola(self): 
        print(len(self.cola)) 
    def vaciar_cola(self): 
        self.cola.clear() 
    def mostrar_estado_cola(self): 
        print(self.cola) 


