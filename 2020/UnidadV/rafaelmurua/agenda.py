import sys
import os
import json
lectura2=[]
class Persona:
    # no se olviden del init  para definir los atributos
    def __init__(self):
        pass
    def agregar_datos(self):  # importante
        # guardan en atributos
        self.nombre = input("escribir nombre: ")
        self.apellido = input("escribir apellido: ")
        self.telefono = int(input("escribir telefono: "))
        self.email = input("escribir email: ")
        # ..
        # ..
        pass

class Notas:
    def  __init__(self):
        pass
    def crear_notas(self): # importante
     
        self.nombre = input('Ingrese nombre de la Nota: ')
        self.contenido = input('Ingrese contenido de la Nota: ')
        pass

data = {}
data['personas'] = []
data_note = {}
data_note['notas'] = []

class Memoria:
    # init definir los atributos
    def guardar_informacion_persona(self, persona): #importante
        print('Los datos que se estan guardando son: ' , persona.nombre, persona.apellido, persona.telefono , persona.email) 
        data['personas'].append({
            'nombre': persona.nombre,
            'apellido': persona.apellido,
            'telefono': persona.telefono,
            'email': persona.email})
        with open('personas.json', 'w') as f:
            json.dump(data, f, indent=4)
        pass

    def guardar_informacion_notas(self, nota): #importante
        
        data_note['notas'].append({
            'nombre de la nota': nota.nombre,
            'contenido de la nota': nota.contenido 
        })
        with open('notas.json', 'w') as f:
            json.dump(data_note, f, indent=4)
        pass


    def mostrar_personas(self): # importantes
        with open('personas.json') as f:
            personas = json.load(f)
            print(personas['personas'])
        pass

    def mostrar_notas(self): # importantes
        with open('notas.json') as f:
            notas = json.load(f)
            print(notas['notas'])
        pass


    def modificar_personas(self):
        c = 0
        with open('personas.json') as f:
            personas = json.load(f)
            nombre = input('Ingrese nombre de la persona a editar: ')
            for persona in personas['personas']:
                if nombre in persona['nombre']:
                    persona['nombre'] = input('Ingrese nuevo Nombre: ')
                    persona['apellido'] = input('Ingrese nuevo Apellido: ')
                    persona['telefono'] = input('Ingrese nuevo Telefono: ')
                    persona['email'] = input('Ingrese nuevo Email: ')
                    print('¡Persona Modificada!')
                c+=1
            with open('personas.json', 'w') as f:
                json.dump(personas, f, indent=4)
        pass

    def borrar_personas(self):
        c = 0
        with open('personas.json') as f:
            personas = json.load(f)
            nombre = input('Ingrese nombre de la persona a borrar: ')
            for persona in personas['personas']:
                if nombre in persona['nombre']:
                    personas['personas'].pop(c)
                    print('¡Persona Borrada!')   
                c+=1
            with open('personas.json', 'w') as f:
                json.dump(personas, f, indent=4)
        pass

    def borrar_nota(self):
        c = 0
        with open('notas.json') as f:
            notas = json.load(f)
            nombre = input('Ingrese nombre de la nota a editar: ')
            for nota in notas['notas']:
                if nombre in nota['nombre de la nota']:
                    notas['notas'].pop(c)
                    print('¡Nota Borrada!')
                    
                c+=1
            with open('notas.json', 'w') as f:
                json.dump(notas, f, indent=4)
        
        pass
        

    def modificar_nota(self):
        c = 0
        with open('notas.json') as f:
            notas = json.load(f)
            nombre = input('Ingrese nombre de la nota a editar: ')
            for nota in notas['notas']:
                if nombre in nota['nombre de la nota']:
                    nota['nombre de la nota'] = input('Ingrese nuevo Nombre de la Nota: ')
                    nota['contenido de la nota'] = input('Ingrese nuevo Contenido de la Nota: ')
                    print('¡Nota Modificada!')    
                c+=1
            with open('notas.json', 'w') as f:
                json.dump(notas, f, indent=4)
        pass
        
        
class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'clear')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de personas")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("-------------- BONUS TRACK --------------")
                print("Precione 7 para buscar y modificar la informacion de personas")
                print("Precione 8 para buscar y borrar la informacion de personas")
                print("Precione 9 para buscar y modificar una nota")
                print("Precione 10 para buscar y borrar una nota")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción: ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Modificando información de personas...")
                    self.modificar_personas()
                elif seleccion == 8:
                    print("Borrando información de personas...")
                    self.borrar_personas()
                elif seleccion == 9:
                    print("Modificando contenido de nota...")
                    self.modificar_nota()
                elif seleccion == 10:
                    print("Modificando contenido de nota...")
                    self.borrar_nota()


                else:
                    print("presione una opción válida")
            except ValueError:
                    print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)


    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()


    def mostrar_notas(self):
        self.memoria.mostrar_notas()

    def modificar_personas (self):
        self.memoria.modificar_personas()

    def borrar_personas (self):
        self.memoria.borrar_personas()

    def modificar_nota(self):
        self.memoria.modificar_nota()
    
    def borrar_nota(self):
        self.memoria.borrar_nota()
    

if __name__ == '__main__':
    menu = Menu()
    menu.run()
