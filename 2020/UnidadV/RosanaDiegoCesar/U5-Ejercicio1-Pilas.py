import os
import sys
class Pila:
    def __init__(self):
        self.pila = []
    def agregar_elemento(self, x):
        self.pila.append(x)
    def quitar_elemento(self):
        print(self.pila.pop())
    def tamano_pila(self):
        print(len(self.pila))
    def vaciar_pila(self):
        self.pila.clear()
    def mostrar_estado_pila(self):
        print(self.pila)


class Menu:
    def __init__(self):
        self.pila = []
        self.x = None
    def run(self):
        self.pila = Pila()
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')
                print("Presione 1 para Agregar Elemento a la Pila")
                print("Presione 2 para Quitar Elemento de la Pila")
                print("Presione 3 para Saber el Tamaña de la Pila")
                print("Presione 4 para Vaciar la Pila")
                print("Presione 5 para Mostrar Estado de la Pila")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Agregando Elemento a la Pila...")
                    self.x = input("Ingrese Valor: ")
                    self.pila.agregar_elemento(self.x)
                elif seleccion == 2:
                    print("Quitando Elemento a la Pila...")
                    self.pila.quitar_elemento()
                elif seleccion == 3:
                    print("Tamaño de la Pila...")
                    self.pila.tamano_pila()
                elif seleccion == 4:
                    print("Vaciando la Pila...")
                    self.pila.vaciar_pila()
                elif seleccion == 5:
                    print("Estado de la Pila...")
                    self.pila.mostrar_estado_pila()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")


    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

if __name__ == '__main__':
    menu = Menu()
    menu.run()
