import sys
import os
import json

class Persona:
    # no se olviden del init  para definir los atributos
    # def __init__(self, nombre, apellido, edad, telefono, mail):
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.edad = None
        self.telefono = None
        self.mail = None
        self.confirma = None
        self.indice = None

    def agregar_datos(self):  # importante
        # guardan en atributos
        self.nombre = input("Ingresar Nombre: ")
        self.apellido = input("Ingresar Apellido: ")
        self.edad = input("Ingresar Edad: ")
        self.telefono = input("Ingresar Telefono: ")
        self.mail = input("Ingresar Mail: ")
        #llamar a clase Memoria y su metodo guardar_informacion_persona_alta(self.persona)
        #memoria = Memoria()
        #memoria.guardar_informacion_persona_alta()
        pass

    def editar_datos(self):
        self.mail = input("Mail de la Persona a Editar: ")
        # Buscar correo en la lista de personas. Sino encuentra mostrar mensaje, en el otro caso, continuar.
        # traer registro del email introducido y dentro de cada input poner el valor original del campo.
        # si deja en blanco, es porque no quiere cambiar el dato. Excepto el Email (si quiere cambiar Email, Borrar).
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            for i in range(0, len(cadena['Personas'])):
                if (self.mail in cadena['Personas'][i]['Email']) == True:
                    # Aqui podriamos hacer como en borrar_datos(). O sea, asignar al atributo indice, la
                    # posicion dentro de la lista, donde se encuentra la persona en edicion.
                    # Para que en metodo donde se guardan los cambios a la persona, se tome directamente
                    # el indice de la lista, y evitar tener que recorrerla nuevamente antes de grabar.
                    bandera_email_existe = 1
                    vnombre = cadena['Personas'][i]['Nombre']
                    vapellido = cadena['Personas'][i]['Apellido']
                    vedad = cadena['Personas'][i]['Edad']
                    vtelefono = cadena['Personas'][i]['Telefono']
            if bandera_email_existe==0:
                print("Ingrese un Email existente, para editar la persona")
            else:
                print("Si no desea cambiar el dato, no ingrese ningun valor")
                self.nombre = input(f"Ingresar por el Nombre, {vnombre}: ")
                self.apellido = input(f"Ingresar por el Apellido, {vapellido}: ")
                self.edad = input(f"Ingresar por la Edad, {vedad}: ")
                self.telefono = input(f"Ingresar por el Telefono, {vtelefono}: ")
        pass

    def borrar_datos(self):
        self.mail = input("Mail de la Persona a Borrar: ")
        # Buscar correo en la lista de personas. Sino encuentra mostrar mensaje, en el otro caso, continuar.
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            for i in range(0, len(cadena['Personas'])):
                #if (self.mail in cadena['Personas'][i]['Email']) == True:
                if cadena['Personas'][i]['Email']==self.mail:
                    self.indice=i
                    bandera_email_existe = 1
                    break
            if bandera_email_existe==0:
                print("Ingrese un Email existente, para poder borrar una persona")
            else:
                print("Datos Persona a Borrar:",cadena['Personas'][i])
        pass

class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.contenido_nota = None
        self.nro_nota = None
        self.email = None
        # atributos nuevos, para aplicacion de "metodo corto de actualizaciones"
        self.cadena = None
        self.notas_de_la_persona = None
        self.indice_persona = None
        self.indice_nota = None
        self.permite_grabar_edicion = 0
        #

    def crear_notas(self):  # importante
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()
        pass

    def editar_notas(self):
        # Alternativa 1: Trabajar con personas.txt dentro de este metodo, e impactar el cambio en el archivo fisico, desde aqui mismo.
        self.email = input("Email de la Persona, para editar sus notas: ")
        # presentar lista de Notas de la persona solicitada a traves de su email. Asi se recuerden los nro_nota
        # y se ingrese éste, para la ubicacion de la nota a modificar.
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            contador_notas = 0
            for i in range(0, len(cadena['Personas'])):
                if self.email == cadena['Personas'][i]['Email']:
                    bandera_email_existe = 1
                    # recuperar y mostrar la lista de notas, para la persona indicada.
                    notas_de_la_persona = cadena['Personas'][i]['Notas']
                    print("Notas de la Persona:", cadena['Personas'][i]['Nombre'],
                          cadena['Personas'][i]['Apellido'], "- Email:",
                          cadena['Personas'][i]['Email'])
                    self.indice_persona=i # Esto lo dejamos por si usamos el metodo corto de guardar_informacion_notas_editar
                    self.cadena=cadena
                    self.notas_de_la_persona=notas_de_la_persona
                    for j in range(0, len(notas_de_la_persona)):
                        contador_notas = contador_notas+1
                        print("Nro_Nota", notas_de_la_persona[j]['nro_nota'])
                        print("Titulo", notas_de_la_persona[j]['titulo'])
                        print("Contenido", notas_de_la_persona[j]['contenido'])
                    if contador_notas==0:
                       print("No posee Notas aun. Debe dar de alta para luego poder editar.")
                    break
            if bandera_email_existe==0:
               print("El Email ingresado no existe entre las Personas")
            if contador_notas>0:
               self.nro_nota = int(input("Nro de la Nota a Editar: "))
               bandera_nro_nota_existe = 0
               for k in range(0, len(notas_de_la_persona)):
                   if self.nro_nota == notas_de_la_persona[k]['nro_nota']:
                      bandera_nro_nota_existe = 1
                      vtitulo = notas_de_la_persona[k]['titulo']
                      vcontenido = notas_de_la_persona[k]['contenido']
               if bandera_nro_nota_existe == 0:
                  print("Ingrese un Nro_Nota existente, para editar la Nota")
               else:
                  print("Si no desea cambiar el dato, no ingrese ningun valor")
                  self.nombre_nota = input(f"Ingresar por el Titulo, {vtitulo}: ")
                  self.contenido_nota = input(f"Ingresar por el Contenido, {vcontenido}: ")
                  self.permite_grabar_edicion=1
        pass

    def borrar_notas(self):
        self.email = input("Email de la Persona, para borrar sus notas: ")
        # presentar lista de Notas de la persona solicitada a traves de su email. Asi se recuerden los nro_nota
        # y se ingrese éste, para la ubicacion de la nota a modificar.
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            contador_notas = 0
            for i in range(0, len(cadena['Personas'])):
                if self.email == cadena['Personas'][i]['Email']:
                    bandera_email_existe = 1
                    # recuperar y mostrar la lista de notas, para la persona indicada.
                    notas_de_la_persona = cadena['Personas'][i]['Notas']
                    print("Notas de la Persona:", cadena['Personas'][i]['Nombre'],
                          cadena['Personas'][i]['Apellido'], "- Email:",
                          cadena['Personas'][i]['Email'])
                    self.indice_persona=i # Esto lo dejamos por si usamos el metodo corto de guardar_informacion_notas_editar
                    self.cadena=cadena
                    self.notas_de_la_persona=notas_de_la_persona
                    for j in range(0, len(notas_de_la_persona)):
                        contador_notas = contador_notas+1
                        print("Nro_Nota", notas_de_la_persona[j]['nro_nota'])
                        print("Titulo", notas_de_la_persona[j]['titulo'])
                        print("Contenido", notas_de_la_persona[j]['contenido'])
                    if contador_notas==0:
                       print("No posee Notas aun. Debe dar de alta para luego poder borrar.")
                    break
            if bandera_email_existe==0:
               print("El Email ingresado no existe entre las Personas")
            if contador_notas>0:
               self.nro_nota = int(input("Nro de la Nota a Borrar: "))
               bandera_nro_nota_existe = 0
               for k in range(0, len(notas_de_la_persona)):
                   if self.nro_nota == notas_de_la_persona[k]['nro_nota']:
                      bandera_nro_nota_existe = 1
                      vtitulo = notas_de_la_persona[k]['titulo']
                      vcontenido = notas_de_la_persona[k]['contenido']
                      indice_nota = k
               if bandera_nro_nota_existe == 0:
                  print("Ingrese un Nro_Nota existente, para borrar la Nota")
               else:
                  print("Nro_Nota:",self.nro_nota,"-Titulo:",vtitulo)
                  confirma_borrado = input("Confirma el borrado de la Nota ?S/N:")
                  if confirma_borrado!="S" and confirma_borrado!="N":
                      print("Debe ingresar S ó N")
                  else:
                      if confirma_borrado=="S":
                          self.permite_grabar_edicion=1
                          self.indice_nota=indice_nota
                          self.nombre_nota=vtitulo
                          self.contenido_nota=vcontenido
        pass

    def escribir_contenido_notas(self):
        self.contenido_nota = input("contenido:")
        # nombre de la nota
        pass

    def colocar_nombre_notas(self):
        self.nro_nota = int(input("nro de Nota:"))
        self.nombre_nota = input("nombre de Nota:")
        self.email = input("Email del Emisor:")
        pass


class Memoria:
    #def __init__(self):
        # init definir los atributos
    def guardar_informacion_persona_alta(self, persona):  # importante
    #def guardar_informacion_persona_alta(self):  # importante
        nombre = persona.nombre
        apellido = persona.apellido
        edad = persona.edad
        telefono = persona.telefono
        mail = persona.mail
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            for i in range(0, len(cadena['Personas'])):
                if (mail in cadena['Personas'][i]['Email']) == True:
                    print("Email ya registrado")
                    bandera_email_existe = 1
            if bandera_email_existe == 0:
                alta = {}
                alta['Nombre'] = nombre
                alta['Apellido'] = apellido
                alta['Edad'] = edad
                alta['Telefono'] = telefono
                alta['Email'] = mail
                alta['Notas'] = []
                cadena['Personas'].append(alta)
                with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                    json.dump(cadena, f)
        pass

    # Alternativa 2: Crear otro metodo "Guardar_Edicion_Persona" en la Clase Memoria, y alli trabajar con personas.txt, e impactar el cambio en el archivo fisico, recien.
    #def guardar_informacion_persona_edicion(self,persona):
    def guardar_informacion_persona_edicion(self, persona):
        nombre = persona.nombre
        apellido = persona.apellido
        edad = persona.edad
        telefono = persona.telefono
        mail = persona.mail
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            for i in range(0, len(cadena['Personas'])):
                if (mail in cadena['Personas'][i]['Email']) == True:
                    if str(nombre).strip() != '':
                        cadena['Personas'][i]['Nombre']=nombre
                    if str(apellido).strip() !='':
                        cadena['Personas'][i]['Apellido']=apellido
                    if str(edad).strip() != '':
                        cadena['Personas'][i]['Edad']=edad
                    if str(telefono).strip() !='':
                        cadena['Personas'][i]['Telefono']=telefono
                    break
        with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
             json.dump(cadena, f)
        pass

    def guardar_informacion_persona_borrar(self, persona):
        mail = persona.mail
        indice = persona.indice
        #print("mail:",mail,"indice:",indice,"tipo dato del indice:",type(indice))
        if type(indice)!=int:
            print("Posiblemente el Email señalado para el borrado de la persona, no existe. Debe operar nuevamente")
        else:
            with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
                cadena = json.load(f)
                if (mail in cadena['Personas'][indice]['Email']) == True:
                    print("Borrado de la persona:", cadena['Personas'][indice])
                    # Aqui aplicamos ubicacion de persona con indice traido desde la clase.
                    # Estaria la forma de ubicarse en un elemento de la lista, con indice arrastrado desde la clase,
                    # y la otra forma es la de volver a hacer la busqueda (que ya se hace en el metodo que hace
                    # la toma de los datos y chequeos previos), dentro de este metodo de grabacion en archivo.
                    # Pueden tener sus ventajas y desventajas cada una de las 2 alternativas.
                    # Por ejemplo: Recorrer nuevamente el contenido del archivo, significa mas lineas de codigo,
                    # y un poco mas de tiempo de procesamiento (que se notaria con archivos muy grandes quizas).
                    # La ventaja de hacer el recorrido nuevamente, es que si alguien modifico el orden de los
                    # elementos, puede impactar en el indice, con el rieso de ubicarse en otro elemento. Habria que
                    # revisarlo con mas profundidad.
                    cadena['Personas'].pop(indice)
                """ # Esta es la forma de recorrer nuevamente con el for.
                for i in range(0, len(cadena['Personas'])):
                    if (mail in cadena['Personas'][i]['Email']) == True:
                        print("Eliminar:",cadena['Personas'][i]['Email'])
                        break
                """
            with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                json.dump(cadena, f)
        pass

    def guardar_informacion_notas(self, nota):  # importante
        nro_nota = nota.nro_nota
        nombre_nota = nota.nombre_nota
        contenido_nota = nota.contenido_nota
        mail = nota.email
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            bandera_nro_nota_existe = 0
            for i in range(0, len(cadena['Personas'])):
                if (mail in cadena['Personas'][i]['Email']) == True:
                    bandera_email_existe = 1
                    # y buscar en este array de notas, si existe el nro_nota.
                    notas_de_la_persona = cadena['Personas'][i]['Notas']
                    posicion_persona=i
                    for j in range(0, len(notas_de_la_persona)):
                        if nro_nota == notas_de_la_persona[j]['nro_nota']:
                           bandera_nro_nota_existe = 1
                           print("El Nro de Nota", nro_nota,"ya esta cargado para la persona con email",mail)

            if bandera_email_existe==0:
                print("Debe ingresar un Email que exista para una persona, y asi agregar una nota suya")

            if bandera_email_existe == 1 and bandera_nro_nota_existe == 0:
                print("Alta de Nota, realizada para persona con email", mail)
                alta = {}
                alta['nro_nota'] = nro_nota
                alta['titulo'] = nombre_nota
                alta['contenido'] = contenido_nota
                cadena['Personas'][posicion_persona]['Notas'].append(alta)
                #print("notas de persona con Email:", mail, "=>",cadena['Personas'][posicion_persona]['Notas'])
                with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                    json.dump(cadena, f)
        pass

    def guardar_informacion_notas_editar(self, nota):  # Metodo con diseño ahorrativo en lineas y procesamiento ?
        nro_nota = nota.nro_nota
        nombre_nota = nota.nombre_nota
        contenido_nota = nota.contenido_nota
        email = nota.email
        cadena = nota.cadena
        notas_de_la_persona = nota.notas_de_la_persona
        indice_persona = nota.indice_persona
        permite_grabar_edicion = nota.permite_grabar_edicion
        if permite_grabar_edicion==1:
            print("Edicion de Nota, realizada para persona con email", email)
            for j in range(0, len(notas_de_la_persona)):
                if nro_nota==notas_de_la_persona[j]['nro_nota']:
                    if str(nombre_nota).strip() != '':
                        notas_de_la_persona[j]['titulo'] = nombre_nota
                    if str(contenido_nota).strip() != '':
                        notas_de_la_persona[j]['contenido'] = contenido_nota
                    break
            # cadena['Personas'][indice_persona]['Notas'].append(notas_de_la_persona)
            cadena['Personas'][indice_persona]['Notas'] = notas_de_la_persona
            with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                json.dump(cadena, f)
        else:
            print("Debe haber completado la edicion en pantalla previa")
        """
        # Esta parte comentada es el diseño con un poco mas de lineas de codigo y quizas para grandes volumenes 
        # de datos, mayor tiempo de procesamiento.
        nro_nota = nota.nro_nota
        nombre_nota = nota.nombre_nota
        contenido_nota = nota.contenido_nota
        mail = nota.email
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            bandera_email_existe = 0
            bandera_nro_nota_existe = 0
            for i in range(0, len(cadena['Personas'])):
                if mail == cadena['Personas'][i]['Email']:
                    bandera_email_existe = 1
                    # y buscar en este array de notas, si existe el nro_nota.
                    notas_de_la_persona = cadena['Personas'][i]['Notas']
                    posicion_persona=i
                    for j in range(0, len(notas_de_la_persona)):
                        if nro_nota == notas_de_la_persona[j]['nro_nota']:
                           bandera_nro_nota_existe = 1
                           print("El Nro de Nota", nro_nota,"ya esta cargado para la persona con email",mail)

            if bandera_email_existe==0:
                print("Debe ingresar un Email que exista para una persona, y asi agregar una nota suya")

            if bandera_email_existe == 1 and bandera_nro_nota_existe == 0:
                print("Alta de Nota, realizada para persona con email", mail)
                alta = {}
                alta['nro_nota'] = nro_nota
                alta['titulo'] = nombre_nota
                alta['contenido'] = contenido_nota
                cadena['Personas'][posicion_persona]['Notas'].append(alta)
                #print("notas de persona con Email:", mail, "=>",cadena['Personas'][posicion_persona]['Notas'])
                with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                    json.dump(cadena, f)
        """
        pass

    def guardar_informacion_notas_borrar(self, nota):  # importante
        nro_nota = nota.nro_nota
        nombre_nota = nota.nombre_nota
        contenido_nota = nota.contenido_nota
        email = nota.email
        cadena = nota.cadena
        notas_de_la_persona = nota.notas_de_la_persona
        indice_persona = nota.indice_persona
        indice_nota = nota.indice_nota
        permite_grabar_edicion = nota.permite_grabar_edicion
        if permite_grabar_edicion==1:
            print("Borrado de Nota, realizado para persona con email", email)
            print("Nro de Nota borrada: ",nro_nota,"Titulo:",nombre_nota,"Contenido:",contenido_nota)
            """
            for j in range(0, len(notas_de_la_persona)):
                if nro_nota==notas_de_la_persona[j]['nro_nota']:
                    notas_de_la_persona.pop(indice_nota)
                    break
            """
            notas_de_la_persona.pop(indice_nota)
            cadena['Personas'][indice_persona]['Notas'] = notas_de_la_persona
            with open("posibleagenda.json", mode='w', encoding='utf-8') as f:
                json.dump(cadena, f)
        else:
            print("Debe haber completado el borrado en pantalla previa")
        pass

    def mostrar_personas(self):  # Recorrer contenido de archivo json, para ir desplegando.
        with open("posibleagenda.json", mode='r', encoding='utf-8') as f:
            cadena = json.load(f)
            for i in range(0, len(cadena['Personas'])):
                print("-------------------------------------------------------------------------------------------------")
                print("Nombre - Apellido - Edad - Telefono - Email")
                print(cadena['Personas'][i]['Nombre'],cadena['Personas'][i]['Apellido'],cadena['Personas'][i]['Edad'],cadena['Personas'][i]['Telefono'],cadena['Personas'][i]['Email'])
                # y buscar en este array de notas, si es que tiene notas.
                notas_de_la_persona = cadena['Personas'][i]['Notas']
                #posicion_persona=i
                print("------------------------------------ Notas ------------------------------------------------------")
                if len(notas_de_la_persona)>0:
                    for j in range(0, len(notas_de_la_persona)):
                        print("Nro_Nota:",notas_de_la_persona[j]['nro_nota'])
                        print("Titulo:", notas_de_la_persona[j]['titulo'])
                        print("Contenido:", notas_de_la_persona[j]['contenido'])
                else:
                    print("No posee Notas aún")
        pass

    #def mostrar_notas_de_una_persona(self):  # para mostrar de una persona en particular

    #    pass


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')

                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las personas")
                #print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para editar una persona")
                print("Presione 8 para editar una nota")
                print("Presione 9 para borrar una persona")
                print("Presione 10 para borrar una nota")
                print("Presione 11 guardar edicion persona")
                print("Presione 12 guardar borrado persona")
                print("Presione 13 guardar edicion nota")
                print("Presione 14 guardar borrado nota")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas con sus notas...")
                    self.mostrar_datos_personas()
                    """
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas de una persona...")
                    self.mostrar_notas_de_una_persona()
                    """
                elif seleccion == 7:
                    print("Editar datos de una persona...")
                    self.editar_datos_persona()
                elif seleccion == 8:
                    print("Editar datos de una nota...")
                    self.editar_datos_nota()
                elif seleccion == 9:
                    print("Borrar una persona...")
                    self.borrar_datos_persona()
                elif seleccion == 10:
                    print("Borrar una nota...")
                    self.borrar_datos_nota()
                elif seleccion == 11:
                    print("Guardar Edicion de persona...")
                    self.guardar_info_persona_edicion()
                elif seleccion == 12:
                    print("Guardar Borrado de persona...")
                    self.guardar_info_persona_borrar()
                elif seleccion == 13:
                    print("Guardar Edicion de Nota...")
                    self.guardar_info_nota_edicion()
                elif seleccion == 14:
                    print("Guardar Borrado de Nota...")
                    self.guardar_info_nota_borrar()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def editar_datos_persona(self):
        self.persona = Persona()
        self.persona.editar_datos()

    def borrar_datos_persona(self):
        self.persona = Persona()
        self.persona.borrar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def editar_datos_nota(self):
        self.nota = Notas()
        self.nota.editar_notas()

    def borrar_datos_nota(self):
        self.nota = Notas()
        self.nota.borrar_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona_alta(self.persona)
        #self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_info_persona_edicion(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona_edicion(self.persona)
        self.persona = None

    def guardar_info_persona_borrar(self):
        if not self.persona:
            print("Por favor, primero realizar la operacion de seleccion de persona a Borrar")
            return False
        self.memoria.guardar_informacion_persona_borrar(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una nota")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def guardar_info_nota_edicion(self):
        if not self.nota:
            print("por favor, primero inciar edicion de una nota")
            return False
        self.memoria.guardar_informacion_notas_editar(self.nota)
        self.nota = None

    def guardar_info_nota_borrar(self):
        if not self.nota:
            print("por favor, primero iniciar borrado de una nota")
            return False
        self.memoria.guardar_informacion_notas_borrar(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    #def mostrar_notas(self):
    #    self.memoria.mostrar_notas_de_una_persona()


if __name__ == '__main__':
    menu = Menu()
    menu.run()
