import os
import sys
class Cola:
    def __init__(self):
        self.cola = []
    def agregar_elemento(self, x):
        self.cola.append(x)
    def quitar_elemento(self):
        print(self.cola.pop(0))
    def tamano_cola(self):
        print(len(self.cola))
    def vaciar_cola(self):
        self.cola.clear()
    def mostrar_estado_cola(self):
        print(self.cola)


class Menu:
    def __init__(self):
        self.cola = []
        self.x = None
    def run(self):
        self.cola = Cola()
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')
                print("Presione 1 para Agregar Elemento a la Cola")
                print("Presione 2 para Quitar Elemento de la Cola")
                print("Presione 3 para Saber el Tamaña de la Cola")
                print("Presione 4 para Vaciar la Cola")
                print("Presione 5 para Mostrar Estado de la Cola")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Agregando Elemento a la Cola...")
                    self.x = input("Ingrese Valor: ")
                    self.cola.agregar_elemento(self.x)
                elif seleccion == 2:
                    print("Quitando Elemento a la Cola...")
                    self.cola.quitar_elemento()
                elif seleccion == 3:
                    print("Tamaño de la Cola...")
                    self.cola.tamano_cola()
                elif seleccion == 4:
                    print("Vaciando la Cola...")
                    self.cola.vaciar_cola()
                elif seleccion == 5:
                    print("Estado de la Cola...")
                    self.cola.mostrar_estado_cola()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")


    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

if __name__ == '__main__':
    menu = Menu()
    menu.run()
