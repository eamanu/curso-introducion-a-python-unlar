import sys
import os
import json


class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None
        self.lista_personas = []
        self.dic_agenda = {}
        self.cargar_datos()

    def cargar_datos(self):
        """Este metodo lo que hace es cargar los datos
        desde el archivo json donde se guardaon los datos
        """
        try:
            with open('personas.json', 'r') as f:
                contenido = f.read()
                # print(contenido)
                self.dic_agenda = json.loads(contenido)
        except FileNotFoundError:
            self.dic_agenda = {"lista_personas": []}

    def agregar_datos(self):  # importante
        # guardan en atributos
        self.nombre = input("Escribir nombre: ")
        self.apellido = input("Escribir apellido: ")
        self.telefono = input("Escribir teléfono: ")
        self.email = input("Escribir email: ")

        self.lista_personas = [self.nombre, self.apellido, self.telefono, self.email]

        self.dic_agenda['lista_personas'].append(self.lista_personas)

    def editar_persona(self, nombre):
        for p in self.dic_agenda["lista_personas"]:
            if p[0] == nombre:
                self.apellido = input("Escribir apellido: ")
                self.telefono = input("Escribir teléfono: ")
                self.email = input("Escribir email: ")
                p[1] = self.apellido
                p[2] = self.telefono
                p[3] = self.email
                self.guardar_persona()
                return True
        print(nombre, "no coincide con ninguna persona guardada")
        return False

    def borrar_persona(self, nombre):
        for i, p in enumerate(self.dic_agenda["lista_personas"]):
            if p[0] == nombre:
                # eliminar la lista de la posición i de dic_agenda["lista_personas"]
                self.dic_agenda['lista_personas'].pop(i)
                self.guardar_persona()
                print("La persona fue eliminada...")
                return True
        print(nombre, "no existe en la agenda")
        return False

    def guardar_persona(self):
        with open('personas.json', 'w') as f:
            f.write(json.dumps(self.dic_agenda))


class Notas:
    def __init__(self):
        self.nombre_notas = None
        self.contenido = None

    def crear_notas(self):  # importante
        self.colocar_nombre_notas()
        self.escribir_contenido_notas()

    def colocar_nombre_notas(self):
        self.nombre_notas = input("Ingrese el nombre de la nota")

    def escribir_contenido_notas(self):
        self.contenido = input("Agregar el contenido: ")


class Memoria:
    def __init__(self):
        pass

    def guardar_informacion_notas(self, nota):  # importante
        with open("notas.txt", 'a') as f:
            f.write(nota.nombre_notas)
            f.write('\n')
            f.write(nota.contenido)
            f.write('\n')
            f.write("-------------------------------------")
            f.write('\n')

    def mostrar_personas(self):  # importantes
        with open("personas.json", 'r') as f:
            d = json.loads(f.read())

        for p in d["lista_personas"]:
            print("Nombre: ", p[0])
            print("Apellido: ", p[1])
            print("telefono: ", p[2])
            print("email: ", p[3])
            print('\n--------------------\n')

    def mostrar_notas(self):  # importantes
        with open("notas.txt", "r") as f:
            print(f.read())


class Menu:
    def __init__(self):
        self.persona = Persona()
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name != 'posix' else 'clear')

                print("Presione 1 para crear y guardar nueva persona")
                print("Presione 2 para crear y guardar nueva nota")
                print("Presione 3 para mostrar todas las personas")
                print("Presione 4 para  mostrar todas las notas")
                print("Presione 5 para editar datos persona")
                print("Presione 6 para borrar persona")
                # descomentar si se hace el bonus track:
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                    self.guardar_info_persona()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                    self.guardar_nota()
                elif seleccion == 3:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()

                elif seleccion == 4:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 5:
                    print("Editar una personas...")
                    self.editar_persona()

                elif seleccion == 6:
                    print("Eliminar una persona...")
                    self.borrar_persona()

                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona.agregar_datos()

    def editar_persona(self):
        nombre = input("Que usuario quiere editar? Escriba el nombre: ")
        self.persona.editar_persona(nombre)

    def borrar_persona(self):
        nombre = input("Que usuario quiere eliminar? Escriba el nombre: ")
        self.persona.borrar_persona(nombre)

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        self.persona.guardar_persona()

    def guardar_nota(self):
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()


if __name__ == '__main__':
    menu = Menu()
    menu.run()
