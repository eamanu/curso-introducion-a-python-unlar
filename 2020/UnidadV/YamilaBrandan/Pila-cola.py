class Pila:
    def __init__(self):
        self.postres = []

    def agregar_elemento(self, elemento):
        self.postres.append(elemento)

    def quitar_elemento(self):
        self.postres.pop()

    def tamano_pila(self):
        print(len(self.postres))

    def borrar_pila(self):
        self.postres.clear()

    def mostrar_pila(self):
        print(self.postres)

pila_postres = Pila()

pila_postres.agregar_elemento("helado")
pila_postres.agregar_elemento("manzana")
pila_postres.agregar_elemento("banana")

pila_postres.tamano_pila()

pila_postres.quitar_elemento()

pila_postres.tamano_pila()
pila_postres.mostrar_pila()


class Cola:
    def __init__(self):
        self.colores=[]

    def agregar_color(self, color):
        self.colores.append(color)
    def quitar_color(self):
        self.colores.pop(0)
    def tamano_cola(self):
        print(len(self.colores))
    def mostrar_cola(self):
        print(self.colores)
    def borrar_cola(self):
        self.colores.clear()

cola_colores= Cola()

cola_colores.agregar_color("rojo")
cola_colores.agregar_color("verde")
cola_colores.agregar_color("rosa fluor")
cola_colores.agregar_color("turquesa")
cola_colores.tamano_cola()
cola_colores.mostrar_cola()
cola_colores.quitar_color()
cola_colores.tamano_cola()
cola_colores.mostrar_cola()
cola_colores.quitar_color()
cola_colores.tamano_cola()
cola_colores.mostrar_cola()
cola_colores.borrar_cola()
cola_colores.tamano_cola()
cola_colores.mostrar_cola()


