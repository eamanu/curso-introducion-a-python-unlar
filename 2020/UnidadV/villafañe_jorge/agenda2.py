import sys
import os
import json
import os.path
import random

random.seed(None)


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


class Persona:
    def __init__(self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None
        # self.datos_persona = {'personas': []}

    def agregar_datos(self):
        self.nombre = input("Ingrese nombre: ")
        self.apellido = input("Ingrese apellido: ")
        self.telefono = input("Ingrese telefono: ")
        self.email = input("Ingrese email: ")


class Notas:
    def __init__(self):
        self.nombre_nota = None
        self.nota = None

    def crear_notas(self):
        self.escribir_contenido_notas()
        self.colocar_nombre_notas()

    def escribir_contenido_notas(self):
        self.nota = input("Escriba la nota: ")

    def colocar_nombre_notas(self):
        self.nombre_nota = input("Escriba el nombre de la nota: ")


class Memoria:
    def guardar_informacion_persona(self, persona):
        if not os.path.isfile("personas.json"):
            lista_personas = {'personas': []}
            with open("personas.json", "w") as f2:
                json.dump(lista_personas, f2)
        if os.stat("personas.json").st_size == 0:
            lista_personas = {'personas': []}
            with open("personas.json", "w") as f2:
                json.dump(lista_personas, f2)

        with open("personas.json", "r") as f:
            aux = str(random.randrange(0, random.randrange(100, 200))) + "-" + str(
                random.randrange(0, random.randrange(100, 200)))
            lista_personas = json.load(f)
        lista_personas['personas'].append({
            'Id': aux,
            'Nombre': persona.nombre,
            'Apellido': persona.apellido,
            'Telefono': persona.telefono,
            'Email': persona.email
        })
        with open("personas.json", "w") as f2:
            json.dump(lista_personas, f2)

    def mostrar_personas(self):
        with open("personas.json", "r") as f:
            personas_dict = json.load(f)
        for persona in personas_dict['personas']:
            salida = f"\nId: {persona['Id']}\nNombre: {persona['Nombre']}\nApellido: {persona['Apellido']}\nTelefono: {persona['Telefono']}\nEmail: {persona['Email']}"
            print(salida + "\n--------------------------------")

    def guardar_informacion_notas(self, nota):
        if not os.path.isfile("notas.json"):
            lista_notas = {'nota': []}
            with open("notas.json", "w") as f2:
                json.dump(lista_notas, f2)
        if os.stat("notas.json").st_size == 0:
            lista_notas = {'nota': []}
            with open("notas.json", "w") as f2:
                json.dump(lista_notas, f2)

        with open("notas.json", "r") as f:
            aux = str(random.randrange(0, random.randrange(100, 200))) + "-" + str(
                random.randrange(0, random.randrange(100, 200)))
            lista_notas = json.load(f)
        lista_notas['nota'].append({
            'Id': aux,
            'Titulo': nota.nombre_nota,
            'Nota': nota.nota
        })
        with open("notas.json", "w") as f2:
            json.dump(lista_notas, f2)

    def mostrar_notas(self):
        with open("notas.json", "r") as f:
            notas_dict = json.load(f)
        for nota in notas_dict['nota']:
            salida = f"\nId: {nota['Id']}\nTitulo: {nota['Titulo']}\nNota: {nota['Nota']}"
            print(salida + "\n--------------------------------")

    @staticmethod
    def editar_informacion_persona():
        verificarid = 0
        veri_nomb = False
        veri_apellido = False
        veri_telefono = False
        veri_email = False

        with open("personas.json", "r") as f:
            lista_personas = json.load(f)
        for persona in lista_personas['personas']:
            salida = f"\nId: {persona['Id']}\nNombre: {persona['Nombre']}\nApellido: {persona['Apellido']}\nTelefono: {persona['Telefono']}\nEmail: {persona['Email']}"
            print(salida + "\n--------------------------------")

        ID = str(input("Por Favor digite el ID de la persona que desea editar-->  "))
        for persona in lista_personas['personas']:
            if persona['Id'] == ID:
                verificarid = 1
                print(color.BOLD + "¡PRESIONE ENTER EN LOS CAMPOS QUE NO DESEA MODIFICAR!" + color.END)
                print(f"Cambiar Nombre : \"{persona['Nombre']}\" a : ", end='\t')
                nombre = str(input())
                print(f"Cambiar Apellido : \"{persona['Apellido']}\" a : ", end='\t')
                apellido = str(input())
                print(f"Cambiar Telefono : \"{persona['Telefono']}\" a : ", end='\t')
                telefono = str(input())
                print(f"Cambiar Email : \"{persona['Email']}\" a : ", end='\t')
                email = str(input())
                if nombre:
                    persona['Nombre'] = nombre
                    veri_nomb = True
                if apellido:
                    persona['Apellido'] = apellido
                    veri_apellido = True
                if telefono:
                    persona['Telefono'] = telefono
                    veri_telefono = True
                if email:
                    persona['Email'] = email
                    veri_email = True

                print(color.BOLD + "DATOS EDITADOS: " + color.END)
                if veri_nomb:
                    print(color.GREEN + f"-->Nombre: {persona['Nombre']}")
                if veri_apellido:
                    print(color.GREEN + f"-->Apellido: {persona['Apellido']}")
                if veri_email:
                    print(color.GREEN + f"-->Email: {persona['Email']}")
                if veri_telefono:
                    print(color.GREEN + f"-->Telefono: {persona['Telefono']}" + color.END)

        print(color.END)

        if verificarid == 0:
            print("Ingrese un ID valido")

        # ALMACENO LOS CAMBIOS

        with open("personas.json", "w") as f2:
            json.dump(lista_personas, f2)

    @staticmethod
    def borrar_informacion_persona():

        contador = 0

        with open("personas.json", "r") as f:
            lista_personas = json.load(f)
        for persona in lista_personas['personas']:
            salida = f"\nId: {persona['Id']}\nNombre: {persona['Nombre']}\nApellido: {persona['Apellido']}\nTelefono: {persona['Telefono']}\nEmail: {persona['Email']}"
            print(salida + "\n--------------------------------")

        ID = str(input("Por Favor digite el ID de la persona que desea Borrar-->  "))
        for persona in lista_personas['personas']:
            if persona['Id'] == ID:
                nombre = persona['Nombre']
                lista_personas['personas'].pop(contador)
            contador += 1

        print(color.RED + f"\"{nombre}\" FUE BORRADO CON EXITO" + color.END)

        with open("personas.json", "w") as f2:
            json.dump(lista_personas, f2)

    @staticmethod
    def editar_informacion_nota():
        verificarid = 0
        veri_titulo = False
        veri_nota = False

        with open("notas.json", "r") as f:
            lista_notas = json.load(f)
        for nota in lista_notas['nota']:
            salida = f"\nId: {nota['Id']}\nTitulo: {nota['Titulo']}\nNota: {nota['Nota']}"
            print(salida + "\n--------------------------------")

        ID = str(input("Por Favor digite el ID de la nota que desea editar-->  "))
        for nota in lista_notas['nota']:
            if nota['Id'] == ID:
                verificarid = 1
                print(color.BOLD + "¡PRESIONE ENTER EN LOS CAMPOS QUE NO DESEA MODIFICAR!" + color.END)
                print(f"Cambiar Titulo de la nota : \"{nota['Titulo']}\" a : ", end='\t')
                titulo = str(input())
                print(f"Cambiar Contenido de la nota : \"{nota['Nota']}\" a : ", end='\t')
                contenido_nota = str(input())

                if titulo:
                    nota['Titulo'] = titulo
                    veri_titulo = True
                if contenido_nota:
                    nota['Nota'] = contenido_nota
                    veri_nota = True

                print(color.BOLD + "DATOS EDITADOS: " + color.END)
                if veri_titulo:
                    print(color.GREEN + f"-->Titulo: {nota['Titulo']}")
                if veri_nota:
                    print(color.GREEN + f"-->Nota: {nota['Nota']}")

        print(color.END)

        if verificarid == 0:
            print("Ingrese un ID valido")

        # ALMACENO LOS CAMBIOS

        with open("notas.json", "w") as f2:
            json.dump(lista_notas, f2)

    @staticmethod
    def borrar_informacion_notas():
        contador = 0

        with open("notas.json", "r") as f:
            lista_notas = json.load(f)
        for nota in lista_notas['nota']:
            salida = f"\nId: {nota['Id']}\nTitulo: {nota['Titulo']}\nApellido: {nota['Nota']}"
            print(salida + "\n--------------------------------")

        ID = str(input("Por Favor digite el ID de la nota que desea Borrar-->  "))
        for nota in lista_notas['nota']:
            if nota['Id'] == ID:
                titulo = nota['Titulo']
                lista_notas['nota'].pop(contador)
            contador += 1

        print(color.RED + f"\"{titulo}\" FUE BORRADO CON EXITO" + color.END)

        with open("notas.json", "w") as f2:
            json.dump(lista_notas, f2)


class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
            try:
                os.system('cls' if os.name == 'net' else 'cls')
                print("")
                print("Presione 1 para crear nueva persona")
                print("Presione 2 para crear nueva nota")
                print("Presione 3 para guardar información de persona")
                print("Presione 4 para guardar información de notas")
                print("Presione 5 para mostrar todas las persona")
                print("Presione 6 para mostrar todas las notas")
                print("Presione 7 para Editar a una persona")
                print("Presione 8 para Borrar a una persona")
                print("Presione 9 para editar Nota")
                print("Presione 10 para borrar Nota")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)

                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                elif seleccion == 3:
                    print("Guardar info de persona...")
                    self.guardar_info_persona()
                elif seleccion == 4:
                    print("Guardar info de notas...")
                    self.guardar_nota()
                elif seleccion == 5:
                    print("Mostrar datos de todas las personas...")
                    self.mostrar_datos_personas()
                elif seleccion == 6:
                    print("Mostrar datos de todas las notas...")
                    self.mostrar_notas()
                elif seleccion == 7:
                    print("Mostrar informacion de personas a editar")
                    self.editar_persona()
                elif seleccion == 8:
                    print("Mostrar informacion de personas a borrar")
                    self.borra_persona()
                elif seleccion == 9:
                    print("Mostrar informacion de nota a editar")
                    self.editar_nota()
                elif seleccion == 10:
                    print("Mostrar informacion de nota a borrar")
                    self.borrar_nota()
                else:
                    print("presione una opción válida")
            except ValueError:
                print("Seleccione una opción válida")
            input("Presione una <ENTER> para continuar")

    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    # agregar datos de persona
    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        if not self.persona:
            print("Por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        if not self.nota:
            print("por favor, primero agregar una persona")
            return False
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None

    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()

    def editar_persona(self):
        self.memoria.editar_informacion_persona()

    def borra_persona(self):
        self.memoria.borrar_informacion_persona()

    def editar_nota(self):
        self.memoria.editar_informacion_nota()

    def borrar_nota(self):
        self.memoria.borrar_informacion_notas()


if __name__ == '__main__':
    menu = Menu()
    menu.run()
