import sys
import os


class Pila:
    def __init__(self):
        self.pila = []

    def agregar_elemento(self, x):
        self.pila.append(x)

    def quitar_elemento(self):
        self.pila.pop()

    def tamano_pila(self):
        print(len(self.pila))

    def vaciar_pila(self):
        self.pila.clear()

    def mostrar_estado_pila(self):
        print(self.pila)


pila = Pila()

pila.agregar_elemento(25)
pila.agregar_elemento(12)
pila.agregar_elemento(32)
pila.agregar_elemento(313)
pila.mostrar_estado_pila()
pila.quitar_elemento()
pila.mostrar_estado_pila()
