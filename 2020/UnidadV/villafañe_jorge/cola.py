class Cola:
    def __init__(self):
        self.cola = []

    def agregar_elemento(self, x):
        self.cola.append(x)

    def quitar_elemento(self):
        self.cola.reverse()
        self.cola.pop()
        self.cola.reverse()

    def tamano_cola(self):
        print(len(self.cola))

    def vaciar_cola(self):
        self.cola.clear()

    def mostrar_estado_cola(self):
        print(self.cola)


cola = Cola()

cola.agregar_elemento(12)
cola.agregar_elemento(13)
cola.agregar_elemento(14)
cola.agregar_elemento(15)
cola.agregar_elemento(16)
cola.mostrar_estado_cola()
cola.quitar_elemento()
cola.quitar_elemento()
cola.mostrar_estado_cola()
