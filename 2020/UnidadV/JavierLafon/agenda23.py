import sys
import os
import json

class Persona:
    def __init__ (self):
        self.nombre = None
        self.apellido = None
        self.telefono = None
        self.email = None
    def agregar_datos(self):
        self.nombre = input("Ingrese Nombre: ")
        self.apellido = input("Ingrese Apellido: ")
        self.telefono = input("Ingrese Telefono: ")
        self.email = input("Ingrese e-mail: ")

class Notas:
    def __init__ (self):
        self.nombre_nota = None
        self.contenido=None
    def crear_notas(self): # importante
        self.colocar_nombre_notas()
        self.escribir_contenido_notas()
    def escribir_contenido_notas(self):
        self.contenido=input("Ingrese Contenido de la Nota:")
    def colocar_nombre_notas(self):
        self.nombre_nota=input("Ingrese Nombre de la Nota:")

class Memoria:
    def __init__ (self):
        self.lista_cargadas = []
        self.reg_cargado = {}
        self.proximo_id = None
        self.registro_leido = {}

    def verifico_persona(self):
        if os.path.isfile('Personas.json'):
            with open('Personas.json', 'r') as archivo: 
                self.lista_cargadas = json.load(archivo)
            return self.lista_cargadas
        

    def grabo_personas(self):
        with open('Personas.json', 'w') as archivo:
            json.dump(self.lista_cargadas , archivo)
        self.lista_cargadas.clear()
    
    def verifico_notas(self):
        if os.path.isfile('Notas.json'):
            with open('Notas.json', 'r') as archivo:
                self.lista_cargadas = json.load(archivo)
            return self.lista_cargadas
        
   
    def grabo_notas(self):
        with open('Notas.json', 'w') as archivo:
            json.dump(self.lista_cargadas , archivo)
        self.lista_cargadas.clear()

    def guardar_informacion_persona(self,persona):
        persona.nombre
        persona.apellido
        persona.telefono
        persona.email
        self.verifico_persona()
        if len(self.lista_cargadas) != 0:
            for indice, elemento in enumerate(self.lista_cargadas): 
                self.proximo_id=indice+1
                proximo_id = self.proximo_id
            reg_cargado = {'Id' : proximo_id,'Nombre' : persona.nombre , 'Apellido' : persona.apellido, 'Telefono' : persona.telefono, 'Email' : persona.email}
        else: 
            reg_cargado = {'Id' : 0,'Nombre' : persona.nombre, 'Apellido' : persona.apellido, 'Telefono' : persona.telefono, 'Email' : persona.email}
        self.lista_cargadas.append(reg_cargado)
        self.grabo_personas()
        self.lista_cargadas.clear() 
        input("Persona guardada Presione <ENTER>")
           
    def modifico_persona(self):
        self.verifico_persona()
        if len(self.lista_cargadas) != 0:
            print("Persona Cargadas.!!")
        
            menu.run()
        for indice, elemento in enumerate(self.lista_cargadas): 
            self.registro_leido=self.lista_cargadas[indice]
            print(self.registro_leido)
        bandera = True
        while bandera is True:
            elijo_registro = input("Ingrese Nro de ID para modificar: ")
            elijo_registro = int (elijo_registro)
            if elijo_registro < 0 or elijo_registro >= indice+1: 
                    print("ID NO ENCONTRADO")
                    bandera = True
            else:
                bandera = False
        
        for key, value in registro_leido.items():
            print("%s --> %s"% (key, value))
        bandera = True
        while bandera is True:
            print("Ingrese Opcion para modificar un dato")
            print("1-Nombre")
            print("2-Apellido")
            print("3-Telefono")
            print("4-E_Mail")
            print("9-No modifica")
            seleccion = input("Seleccione una opción ")
            seleccion = int (seleccion)
            if seleccion == 1:
                print("Nombre: %s"% (registro_leido["Nombre"]))
                registro_leido["Nombre"]=input("Ingrese nuevo nombre: ") # Guardo cambio realizado por usuario en elemento de lista
                bandera = False
            elif seleccion == 2:
                print("Apellido: %s"% (registro_leido["Apellido"]))
                registro_leido["Apellido"]=input("Ingrese nuevo apellido: ")
                bandera = False
            elif seleccion == 3:
                print("Telefono: %s"% (registro_leido["Telefono"]))
                registro_leido["Telefono"]=input("Ingrese nuevo telefono: ")
                bandera = False
            elif seleccion == 4:
                print("E-mail: %s"% (registro_leido["Email"]))
                registro_leido["Email"]=input("Ingrese nuevo E-Mail: ")
                bandera = False
            elif seleccion == 9:
                input("Modificaciones NO GUARDADAS / Presione <ENTER> para continuar")
                menu.run()
            else:
                print("presione una opción válida")
                bandera = True
        self.lista_cargadas[elijo_registro] = registro_leido # Reemplazo con dato modificado por usuario
        self.grabo_personas()
        self.lista_cargadas.clear()
        input("Modificaciones GUARDADAS / Presione <ENTER> para continuar")

    def borrar_personas (self):
        self.verifico_persona()
        if len(self.lista_cargadas) != 0:
            print("Persona Cargadas....")
        else:
            input("No hay personas guardadas / Presione <ENTER> para volver al menu principal")
            menu.run()
        for indice, elemento in enumerate(self.lista_cargadas): # Recorro e imprimo registros cargados.
            self.registro_leido=self.lista_cargadas[indice]
            print(self.registro_leido)
        bandera = True
        print("Indice: %s" %(indice))
        while bandera is True:
            elijo_registro = input("Ingrese Nro de ID para (BORRAR) : ")
            elijo_registro = int (elijo_registro)
            if elijo_registro < 0 or elijo_registro >= indice+1:
                    print("ID Fuera de Rango.!!)
                    bandera = True
            else:
                bandera = False
        registro_leido=self.lista_cargadas[elijo_registro]
        print("Registro Elegido")
        for key, value in registro_leido.items():
            print("%s --> %s"% (key, value))
        self.lista_cargadas.pop(elijo_registro)
        self.grabo_personas()
        self.lista_cargadas.clear()
        input("Persona BORRADA / Presione <ENTER> para continuar")

    def mostrar_personas(self): # importantes
        self.verifico_persona()
        if len(self.lista_cargadas) != 0:
            for indice, elemento in enumerate(self.lista_cargadas):
                self.registro_leido=self.lista_cargadas[indice]
                print(self.registro_leido)
            self.lista_cargadas.clear()
            input("Presione <ENTER> para continuar")
        else:
            input("No hay personas guardadas")
            menu.run()

    def guardar_informacion_notas(self, nota): #importante
        nota.nombre_nota
        nota.contenido
        self.verifico_notas()
        if len(self.lista_cargadas) != 0:
            for indice, elemento in enumerate(self.lista_cargadas): # Recorro para saber el ultimo ID cargado, y agregar los datos cargados al final de la lista
                self.proximo_id=indice+1
                proximo_id = self.proximo_id
            reg_cargado = {'Id' : proximo_id,'Nombre' : nota.nombre_nota , 'Contenido' : nota.contenido}
        else: 
            reg_cargado = {'Id' : 0,'Nombre' : nota.nombre_nota , 'Contenido' : nota.contenido}
        self.lista_cargadas.append(reg_cargado)
        self.grabo_notas()
        self.lista_cargadas.clear()
        input("Nota Grabada... Presione <ENTER>")

    def modifico_nota(self):
        self.verifico_notas()
        if len(self.lista_cargadas) != 0:
            print("Notas Cargadas.!!")
        else:
            input("No hay notas guardadas")
            menu.run()
        for indice, elemento in enumerate(self.lista_cargadas): # Recorro e imprimo para saber cantidad de registros cargados.
            self.registro_leido=self.lista_cargadas[indice]
            print(self.registro_leido)
        bandera = True
        while bandera is True:
            elijo_registro = input("Ingrese Nro de registro: ")
            elijo_registro = int (elijo_registro)
            if elijo_registro < 0 or elijo_registro >= indice+1:
                    print("Registro Fuera de Rango... Verifique valor ingresado")
                    bandera = True
            else:
                bandera = False
        registro_leido=self.lista_cargadas[elijo_registro]
        print("Registro Elegido")
        for key, value in registro_leido.items():
            print("%s --> %s"% (key, value))
        bandera = True
        while bandera is True:
            print("Ingrese Opcion para modificar un dato")
            print("1 - Nombre Nota")
            print("2 - Contenido Nota")
            print("9 - No modifica")
            seleccion = input("Seleccione una opción ")
            seleccion = int (seleccion)
            if seleccion == 1:
                print("Nombre Nota: %s"% (registro_leido["Nombre"]))
                registro_leido["Nombre"]=input("Ingrese nuevo nombre de nota: ")
                bandera = False
            elif seleccion == 2:
                print("Contenido Nota: %s"% (registro_leido["Contenido"]))
                registro_leido["Contenido"]=input("Ingrese nuevo contenido: ")
                bandera = False
            elif seleccion == 9:
                input("Modificaciones NO GUARDADAS / Presione <ENTER> para continuar")
                menu.run()
            else:
                print("Presione una opción válida")
                bandera = True 
        self.lista_cargadas[elijo_registro] = registro_leido # Reemplazo con dato modificado por usuario
        self.grabo_notas()
        self.lista_cargadas.clear()
        input("Modificaciones GUARDADAS / Presione <ENTER> para continuar")
    
    def borro_nota(self):
        self.verifico_notas()
        if len(self.lista_cargadas) != 0:
            print("Notas Cargadas....")
        else:
            input("No hay Notas guardadas / Presione <ENTER> para volver al menu principal")
            menu.run()
        for indice, elemento in enumerate(self.lista_cargadas): # Recorro e imprimo registros cargados.
            self.registro_leido=self.lista_cargadas[indice]
            print(self.registro_leido)
        bandera = True
        while bandera is True:
            elijo_registro = input("Ingrese Nro de ID para (BORRAR) : ")
            elijo_registro = int (elijo_registro)
            if elijo_registro < 0 or elijo_registro >= indice+1:
                    print("ID Fuera de Rango... Verifique valor ingresado")
                    bandera = True
            else:
                bandera = False
        registro_leido=self.lista_cargadas[elijo_registro]
        print("Registro Elegido")
        for key, value in registro_leido.items():
            print("%s --> %s"% (key, value))
        self.lista_cargadas.pop(elijo_registro)
        self.grabo_cambios_notas()
        self.lista_cargadas.clear()
        input("Nota BORRADA / Presione <ENTER> para continuar")
    
    def mostrar_notas(self): # importantes
        self.verifico_apertura_notas()
        if len(self.lista_cargadas) != 0:
            print("Leo archivo y muestro registros...")
            for indice, elemento in enumerate(self.lista_cargadas):
                self.registro_leido=self.lista_cargadas[indice]
                print(self.registro_leido)
            self.lista_cargadas.clear()
            input("Presione <ENTER> para continuar")
        else:
            input("No hay notas guardadas / Presione <ENTER> para volver al menu principal")
            menu.run()
    

class Menu:
    def __init__(self):
        self.persona = None
        self.nota = None
        self.memoria = Memoria()

    def run(self):
        while True:
             try:
                os.system('cls' if os.name == 'nt' else 'clear')

                print("(1) Crear y guardar nueva persona")
                print("(2) Crear y guardar nueva nota")
                print("(3) Modificar Persona")
                print("(4) Modificar Nota")
                print("(5) Borrar Persona")
                print("(6) Borrar Nota")
                print("(7) Mostrar Personas")
                print("(8) Mostrar Notas")
                print("Presione 'q' para salir")

                seleccion = input("Seleccione una opción ")
                seleccion = self.stop(seleccion)
             
                if seleccion == 1:
                    print("Creando una persona nueva...")
                    self.agregar_datos_personas()
                    self.guardar_info_persona()
                elif seleccion == 2:
                    print("Creando nota...")
                    self.agregar_notas()
                    self.guardar_nota()
                elif seleccion == 3:
                    self.modifico_persona()
                elif seleccion == 4:
                    self.modifico_nota()
                elif seleccion == 5:
                    self.borro_persona()
                elif seleccion == 6:
                    self.borro_nota()
                elif seleccion == 7:
                    print("Mostrar los datos de todas las personas.!")
                    self.mostrar_datos_personas()
                elif seleccion == 8:
                    print("Mostrar contenido de todas las notas.!")
                    self.mostrar_notas()
                else:
                    print("presione una opción válida")
             except ValueError:
                      print("Seleccione una opción válida")
        input("Presione una <ENTER> para continuar")
    
    @staticmethod
    def stop(seleccion):
        if seleccion.lower() == 'q':
            print("bye bye")
            sys.exit(0)
        return int(seleccion)

    def agregar_datos_personas(self):
        self.persona = Persona()
        self.persona.agregar_datos()

    def agregar_notas(self):
        self.nota = Notas()
        self.nota.crear_notas()

    def guardar_info_persona(self):
        self.memoria.guardar_informacion_persona(self.persona)
        self.persona = None

    def guardar_nota(self):
        self.memoria.guardar_informacion_notas(self.nota)
        self.nota = None
    
    def modifico_persona(self):
        self.memoria.modifico_persona()

    def modifico_nota(self):
        self.memoria.modifico_nota()        

    def borrar_personas(self):
        self.memoria.borrar_personas()

    def borro_nota(self):
        self.memoria.borro_nota()
    
    def mostrar_datos_personas(self):
        self.memoria.mostrar_personas()

    def mostrar_notas(self):
        self.memoria.mostrar_notas()

if __name__ == '__main__':
    menu = Menu()
    menu.run()
