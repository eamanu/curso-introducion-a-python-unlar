# coding=utf-8

#Ejercicio 1

def numeros_pares(desde, hasta):
    i= desde
    with open("Ejercicio1.txt", "w") as f:
        while(i!= hasta):
            if i % 2==0:
                print(i)
                f.write(str(i))
                f.write('\n')

            i += 1
print("Ejercicio 1")
desde= int(input("Ingrese el valor inicial: "))
hasta= int(input("Ingrese el valor final: "))
numeros_pares(desde, hasta)
print("Fin Ejercicio 1\n\n\n")

#Ejercicio 2

def numeros_impares(desde, hasta):
    with open("Ejercicio2.txt", "w") as f:
        for i in range(desde, hasta):
            if i% 2 == 1:
                print(i)
                f.write(str(i))
                f.write('\n')
print("Ejercicio 2")
desde= int(input("Ingrese el valor inicial: "))
hasta= int(input("Ingrese el valor final: "))
numeros_impares(desde, hasta)
print("Fin Ejercicio 2\n\n\n")

#Ejercicio 3
def num_tabla (valor):
    with open("Ejercicio3.txt", "w") as f:
        for i in range(0, 11):
            print(valor,"x",i,"=",(valor*i), sep="\t")
            f.write(str(valor))
            f.write("\t")
            f.write("x")
            f.write("\t")
            f.write(str(i))
            f.write("\t")
            f.write("=")
            f.write("\t")
            f.write(str(i*valor))
            f.write('\n')

valor= int(input("Que tabla quiere imprimir? "))
num_tabla(valor)
print("Fin Ejercicio 3\n\n\n")


#Ejercicio 4
def num_primo(desde, hasta):
    with open("Ejercicio4.txt", "w") as f:
        for i in range (desde, hasta):
            for j in range(desde, hasta):
                if (i % j == 0):
                    if i==j:
                        print(i, "es primo")
                        f.write(str(i))
                        f.write('\n')
                    break

print("Ejercicio 4")
desde = int(input("Ingrese el valor inicial: "))
hasta = int(input("Ingrese el valor final: "))
num_primo(desde, hasta)
print("Fin Ejercicio 4\n\n\n")



#Ejercicio 5
def num_pi():

    print("Vamos a encontrar el numero PI usando un FOR yeah!!!", end='\n\n')

    n = 10000000
    calc = 0
    for i in range(1, n + 1):
        calc += (-1)**(i + 1) / (2 * i - 1)
        resultado = 4 * calc
    print("El valor del número PI es:", resultado, end='\n\n')
    print("FIN")
    with open("Ejercicio5.txt", "w") as f:
        f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!")
        f.write('\n')
        f.write('\n')
        f.write("El valor del número PI es: ")
        f.write(str(resultado))
        f.write('\n\n')
        f.write("FIN")


num_pi()

#Ejercicio 6:
def cobrar_descuento(precio, descuento):
    valor_descuento = (precio * descuento)/100
    precio_descuento = precio - valor_descuento
    print(precio_descuento, "$", descuento, "%")
    with open("Ejercicio6.txt", "w") as f:
        f.write("Se debe cobrar $")
        f.write(str(precio_descuento))
        f.write(" con el descuento de ")
        f.write(str(descuento))
        f.write("%.")

print("Ejercicio 6")
precio = int(input("Ingrese el precio: "))
descuento = int(input("Ingrese el descuento: "))

cobrar_descuento(precio, descuento)


#Ejercicio 7:
def mes_año(num_mes):
    if num_mes == 1:
        print("Enero")
    elif num_mes == 2:
        print("Febrero")
    elif num_mes == 3:
        print("Marzo")
    elif num_mes == 4:
        print("Abril")
    elif num_mes == 5:
        print("Mayo")
    elif num_mes == 6:
        print("Junio")
    elif num_mes == 7:
        print("Julio")
    elif num_mes == 8:
        print("Agosto")
    elif num_mes == 9:
        print("Septiembre")
    elif num_mes == 10:
        print("Octubre")
    elif num_mes == 11:
        print("Noviembre")
    elif num_mes == 12:
        print("Diciembre")
    else:
        print("El número no corresponde a un mes del calendario")

num_mes = int(input("Ingrese el número del mes: "))
mes_año(num_mes)