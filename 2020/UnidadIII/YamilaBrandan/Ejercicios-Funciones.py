#Ejercicio 1
def num_par(desde, hasta):
    for i in range (desde, hasta):
        if (i % 2) == 0:
            print (i)

num_par(1, 10)

#Ejercicio 2
def num_impar(desde, hasta):
    for i in range (desde, hasta):
        if (i % 2) != 0:
            print(i)

num_impar(3, 12)

#Ejercicio 3
def num_tabla (valor):

    for i in range(0, 11):
        print(valor,"x",i,"=",(valor*i),sep="\t")

num_tabla(2)

#Ejercicio 4

def num_acumulado(valor_primer_if_hasta,
                  valor_segundo_if_desde,
                  valor_segundo_if_hasta):
    acu = 0
    for i in range(valor_primer_if_hasta):
        if i<valor_segundo_if_desde:
            continue
        for j in range(valor_segundo_if_desde, valor_segundo_if_hasta):
            if (i==j):
                acu = acu+1
                break
    print(acu)

num_acumulado(100, 23, 200)

#Ejercicio 5
def num_primo(desde_primer_if, hasta_primer_if,
              desde_segundo_if, hasta_segundo_if):
    for i in range (desde_primer_if, hasta_primer_if):
        for j in range(desde_segundo_if, hasta_segundo_if):
            if (i % j == 0):
                if i==j:
                    print(i, "es primo")
                break

num_primo(2, 1000, 2, 1000)

#Ejercicio 6
def num_pi():
    print("Vamos a encontrar el numero PI usando un FOR yeah!!!", end='\n\n')
    n = 10000000
    calc = 0
    for i in range(1, n + 1):
        calc += (-1)**(i + 1) / (2 * i - 1)
        resultado = 4 * calc
    print("El valor del número PI es:", resultado, end='\n\n')
    print("FIN")


num_pi()

#Ejercicio 7:
def descuento(precio, descuento):
    valor_descuento = (precio * descuento)/100
    precio_descuento = precio - valor_descuento
    print(precio_descuento, "$", descuento, "%")

descuento(1000, -2.5)

#Ejercicio 8:
def mes_año(num_mes):
    if num_mes == 1:
        print("Enero")
    elif num_mes == 2:
        print("Febrero")
    elif num_mes == 3:
        print("Marzo")
    elif num_mes == 4:
        print("Abril")
    elif num_mes == 5:
        print("Mayo")
    elif num_mes == 6:
        print("Junio")
    elif num_mes == 7:
        print("Julio")
    elif num_mes == 8:
        print("Agosto")
    elif num_mes == 9:
        print("Septiembre")
    elif num_mes == 10:
        print("Octubre")
    elif num_mes == 11:
        print("Noviembre")
    elif num_mes == 12:
        print("Diciembre")
    else:
        print("El número no corresponde a un mes del calendario")


mes_año(1)