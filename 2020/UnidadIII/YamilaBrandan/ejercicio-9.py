with open("adn_hebra.txt", "r") as leer:
    with open("adn_complementario", "w") as escribir:
        for base in leer.read():
            if base=="a":
                escribir.write("a - t\n")
            elif base=="t":
                escribir.write("t - a\n")
            elif base=="c":
                escribir.write("c - g\n")
            elif base=="g":
                escribir.write("g - c\n")
            else:
                print("Base incorrecta")