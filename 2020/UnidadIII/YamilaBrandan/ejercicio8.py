from http import client

def web_response(nombre_pagina, response):
    if response.status == 200:
        print(nombre_pagina, " anda bien!!!")
    else:
        print(nombre_pagina, " esta caido? Error: ", response.reason)

def get_connections(nombre_pagina, puerto, timeout):
    con = client.HTTPConnection(nombre_pagina, puerto, timeout=timeout)
    con.request("GET", "/")
    response = con.getresponse()
    print(response.status, response.reason)

    data = response.read()
    print(data)

    web_response(nombre_pagina, response)



get_connections("www.python.org", 80, 10)
get_connections("www.google.com", 80, 10)
get_connections("www.facebook.com", 80, 10)
get_connections("www.twitter.com", 80, 10)
