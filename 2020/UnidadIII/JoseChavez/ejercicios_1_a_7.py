#Ejercicio 1

def nros_pares(desde, hasta):
    for pares in range(desde,hasta):
        if pares % 2 == 0:
            print(pares)
print("|| Numeros Pares ||")
desde = int(input("Ingrese el rango inicial: "))
hasta = int(input("Ingrese el rango final: "))
nros_pares(desde,hasta)
print()

#Ejercicio 2

def nros_impares(desde_i, hasta_i):
    for impares in range(desde_i,hasta_i):
        if impares % 2 == 1:
            print(impares)

print("|| Numeros Impares ||")

desde_i = int(input("Ingrese el rango inicial: "))
hasta_i = int(input("Ingrese el rango final: "))
nros_impares(desde_i,hasta_i)

#Ejercicio 3

def tabla(nrox):
    with open("ejercicio3.txt",'w') as f:
        for i in range(11):
            print(nrox, "x", i, "=", (nrox * i), sep="\t")
            f.write(str(nrox))
            f.write("\tx\t")
            f.write(str(i))
            f.write("\t=\t")
            f.write(str(nrox * i))
            f.write('\n')

print("\n|| Tabla de multiplicar ||")
nrox = int(input("Ingrese un número del 1 al 10, para mostrar su tabla de multiplicar: "))
tabla(nrox)
print("Archivo ejercicio3.txt grabado")


#Ejercicio 4

def primos(desde_pr, hasta_pr):
    with open("ejercicio4.txt", 'w') as f:
        cont = 0
        desde_pr = int(input("Ingrese el rango inicial: "))
        hasta_pr = int(input("Ingrese el rango final: "))
        for n in range(desde_pr, hasta_pr + 1):
            for d in range(1, n + 1):
                if n % d == 0:
                    cont += 1
            if cont == 2:
                print(n, end="\n")
                f.write(str(n))
                f.write('\n')
            cont = 0

print("\n|| Números primos ||")
primos(desde_pr=5, hasta_pr=1000)
print("Archivo ejercicio4.txt grabado")


#Ejercicio 5

def numero_pi():
    with open("ejercicio5.txt", 'w') as f:
        print("Vamos a encontrar el numero PI usando un FOR yeah!!!\n")
        n = 10000000
        calc = 0
        for i in range(1, n + 1):
            calc += (-1)**(i + 1) / (2 * i - 1)
            resultado = 4 * calc
        print("El valor del numero PI es: ",resultado,"\n\nFIN")
        f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!\n\n")
        f.write("El valor del numero PI es: ")
        f.write(str(resultado))
        f.write("\n\n")
        f.write("FIN")

print("\n|| Número pi ||")
numero_pi()
print("\nArchivo ejercicio5.txt grabado")


#Ejercicio 6

def descuento(precio,desc):
    with open("ejercicio6.txt",'w') as f:
        rdo = (precio*desc)/100
        print("Se debe cobrar $",rdo, " con el descuento de ",desc,"%", sep="")
        f.write("Se debe cobrar $")
        f.write(str(rdo))
        f.write(" con el descuento del ")
        f.write(str(desc))
        f.write("%")
print("\n|| Precio con descuento ||")
precio = int(input("Ingrese el precio del producto: "))
desc = int(input("Ingrese el descuento: "))

descuento(precio,desc)
print("\nArchivo ejercicio6.txt grabado")


#Ejercicio 7

def meses(nro_mes):

    while nro_mes >= 1 and nro_mes <= 12:

        if nro_mes <= 1:
            print("Enero")
            break
        elif nro_mes <= 2:
            print("Febrero")
            break
        elif nro_mes <= 3:
            print("Marzo")
            break
        elif nro_mes <= 4:
            print("Abril")
            break
        elif nro_mes <= 5:
            print("Mayo")
            break
        elif nro_mes <= 6:
            print("Junio")
            break
        elif nro_mes <= 7:
            print("Julio")
            break
        elif nro_mes <= 8:
            print("Agosto")
            break
        elif nro_mes <= 9:
            print("Septiembre")
            break
        elif nro_mes <= 10:
            print("Octubre")
            break
        elif nro_mes <= 11:
            print("Noviembre")
            break
        elif nro_mes <= 12:
            print("Diciembre")
            break
    else:
        print("El valor ingresado es incorrecto!")

print("\n|| Meses ||")
nro_mes = int(input("Ingrese un numero entre 1 y 12: "))
meses(nro_mes)