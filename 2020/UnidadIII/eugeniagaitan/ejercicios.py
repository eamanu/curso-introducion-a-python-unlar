# Ejercicio 1 Números pares

def num_pares (desde, hasta):
    with open ("ejercicio1.txt", 'w') as f:
        for i in range(desde, hasta):
            if(i % 2) == 0 :
                print(i)
                f.write(str(i))

desde = int(input("Escriba el primer rango de numeros pares a encontrar: "))
hasta = int(input("Escriba el segundo rango de numeros pares a encontrar: "))

num_pares(desde, hasta)

# Ejercicio 2 Números impares

def num_impar(desde, hasta):
    for i in range(desde, hasta):
        if (i % 2) != 0 :
            print(i)

desde = int(input("Escriba el primer rango de numeros pares a encontrar: "))
hasta = int(input("Escriba el primer rango de numeros pares a encontrar: "))

num_impar(desde, hasta)

# Ejercicio 3 Tabla de multiplicar ¡¡ sin aplicar el sep !!

def calcular_tabla(num):
    for i in range(11):
        e = (i * num)
        print(num,'\t',"x",'\t',i,'\t',"=",'\t', e)

num = int(input("Escriba el número de la tabla que quiere calcular: "))

calcular_tabla(num)

# Ejercicio 4 Números primos ¡¡ falta en si toda la funcion, tenemos toda la estructura !!

def num_primos(desde, hasta):
    for i in range(desde, hasta):
        if i >= 2 :
            print(i)
desde = int(input("Escribir el primer rango de números primos a encontrar: "))
hasta = int(input("Escribir el segundo rango de números primos a encontrar: "))

num_primos(desde, hasta)

# Ejercicio 5 Calcular número PI ¡¡¡ hasta aquí llegamos :( !!

print("Vamos a encontrar el numero PI usando un FOR yeah!!!\n") 

def calcular_pi():
    n = 10000000

    calc = 0

    for i in range(1, n + 1):
            calc += (-1)**(i + 1) / (2 * i - 1) 
    resultado = (4 * calc)

    print ("El  valor del número PI es:", resultado, "\n\nFIN")

calcular_pi()

# Ejercicio 6 Descuento de producto ¡¡ el descuento no ingresa con el % !!

def descuento_de_producto(precio, descuento):
    desc = (descuento * precio) / 100
    total = (precio - desc)
    print("Se debe cobrar $", total, "con el descuento de", descuento, "%")

precio = int(input("Ingresar precio del producto: "))
descuento = int(input("Ingresar el descuento: "))

descuento_de_producto(precio, descuento)

# Ejercicio 7 Número entero y meses

def meses(numero):
    if numero == 1 :
        print("Enero")
    elif numero == 2 :
        print("Febrero")
    elif numero == 3 :
        print("Marzo")
    elif numero == 4 :
        print("Abril")
    elif numero == 5 :
        print("Mayo")
    elif numero == 6 :
        print("Junio")
    elif numero == 7 :
        print("Julio")
    elif numero == 8 :
        print("Agosto")
    elif numero == 9 :
        print("Septiembre")
    elif numero == 10 :
        print("Octubre")
    elif numero == 11 :
        print("Noviembre")
    elif numero == 12 :
        print("Diciembre")
    elif numero >= 13 :
        print("Error")

numero = int(input("Ingrese un número entero: "))

meses(numero)