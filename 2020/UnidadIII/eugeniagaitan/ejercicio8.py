# Ejercicio 8: intentamos tomar la estructura de otros ejercicios :)

from http import client

def python():
    con = client.HTTPConnection("www.python.org", 80, timeout=10)
    con.request("GET", "/")
    response_python = con.getresponse()
    print(response_python.status, response_python.reason)

    if response_python.status == 200:
        print("Python anda bien!!!")
    else:
        print("Python está caido? Error: ", response_python.reason)

    data = response_python.read()
    print(data)
python()

def google():
    con = client.HTTPConnection("www.google.com", 80, timeout=10)
    con.request("GET", "/")
    response_google = con.getresponse()
    print(response_google.status, response_google.reason)

    if response_google.status == 200:
        print("Google anda bien!!!")
    else:
        print("Google está caido? Error: ", response_google.reason)

    data = response_google.read()
    print(data)
google()

def facebook():
    con = client.HTTPConnection("www.faceook.com", 80, timeout=10)
    con.request("GET", "/")
    response_facebook = con.getresponse()
    print(response_facebook.status, response_facebook.reason)

    if response_facebook.status == 200:
        print("Facebook anda bien!!!")
    else:
        print("Facebook está caido? Error: ", response_facebook.reason)

    data = response_facebook.read()
    print(data)
facebook()

def twitter():
    con = client.HTTPConnection("www.twitter.com", 80, timeout=10)
    con.request("GET", "/")
    response_twitter = con.getresponse()
    print(response_twitter.status, response_twitter.reason)

    if response_twitter.status == 200:
        print("Twitter anda bien!!!")
    else:
        print("Twitter está caido? Error: ", response_twitter.reason)

    data = response_twitter.read()
    print(data)
twitter()