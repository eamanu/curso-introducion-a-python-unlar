# EJERCICIO 1 - CALCULO DE NUMEROS PARES"""

print('\nEJERCICIO 1 - CALCULO DE NUMEROS PARES\n')
start = int(input("Ingrese el numero inicial: "))
stop = int(input("Ingrese el numero final: "))

def num_par(start, stop):
    with open("ejercicio1.txt", 'w') as npar:
        for i in range(start, stop):
            if i % 2 == 0:
                print(i, file=npar, sep='\n')


num_par(start, stop)

# EJERCICIO 2 - CALCULO DE NUMEROS IMPARES

print('\nEJERCICIO 2 - CALCULO DE NUMEROS IMPARES\n')
start1 = int(input("Ingrese el numero inicial: "))
stop1 = int(input("Ingrese el numero final: "))


def num_impar(start1, stop1):
    with open('ejercicio2.txt', 'w') as nimpar:
        for i in range(start, stop):
            if i % 2 != 0:
                print(i, file=nimpar, sep='\n')


num_impar(start1, stop1)

# EJERCICIO 3 - CALCULO DE LA TABLA DEL 9

print('\nEJERCICIO 3 - CALCULO DE LA TABLA DEL 9\n')
n = 11


def calcular_tabla9(n):
    with open('ejercicio3.txt', 'w') as tabla:
        for i in range(n):
            print('9', 'x', str(i), '=', i * 9, sep='\t', file=tabla)


calcular_tabla9(n)

# EJERCICIO 4 - CALCULO DE NUMEROS PRIMOS

print('\nEJERCICIO 4 - CALCULO DE NUMEROS PRIMOS\n')


def num_primos():
    with open('ejercicio4.txt', 'w') as nprimo:
        for i in range(2, 10000):
            es_primo = True
            for j in range(2, i):
                if i % j == 0:
                    es_primo = False
            if es_primo:
                print(i, file=nprimo, sep='\n')

num_primos()

# EJERCICIO 5 - CALCULO DE PI

print('\nEJERCICIO 5 - CALCULO DE PI\n')
n = int(input("Ingrese el numero de muestras para obtener PI: "))
calc = 0


def pi(n, calc):
    with open('ejercicio5.txt', 'w') as numpi:
        for i in range(1, n + 1):
            calc += (-1) ** (i + 1) / (2 * i - 1)
        resultado = 4 * calc
        print('Vamos a encontrar el numero PI usando un FOR yeah!!!', 'El valor del numero Pi es: ', resultado,
              file=numpi, sep='\n')


pi(n, calc)

# EJERCICIO 6 - CALCULO DE PRECIO/DESCUENTO

print('\nEJERCICIO 6 - CALCULO DE PRECIO/DESCUENTO\n')
precio = int(input("Ingrese el precio del producto: "))
des = int(input("Ingrese el descuento a realizar: "))


def descuento(precio, des):
    with open('ejercicio6.txt', 'w') as desc:
        nuevo_precio = precio * des / 100
        print('Se debe cobrar $', nuevo_precio, 'con el descuento de', des, '%.', file=desc)


descuento(precio, des)

# EJERCICIO 7 - CALCULO DE MES

print('\nEJERCICIO 7 - CALCULO DE MES\n')
num = int(input("Ingrese el numero del mes a buscar: "))


def mes(num):
    with open('ejercicio7.txt', 'w') as month:
        if num > 12 or num < 1:
            print('El numero ingresado', num, 'no corresponde a un mes del calendario.', file=month)
        elif num == 1:
            print('El numero ingresado', num, 'corresponde al mes de Enero.', file=month)
        elif num == 2:
            print('El numero ingresado', num, 'corresponde al mes de Febrero.', file=month)
        elif num == 3:
            print('El numero ingresado', num, 'corresponde al mes de Marzo.', file=month)
        elif num == 4:
            print('El numero ingresado', num, 'corresponde al mes de Abril.', file=month)
        elif num == 5:
            print('El numero ingresado', num, 'corresponde al mes de Mayo.', file=month)
        elif num == 6:
            print('El numero ingresado', num, 'corresponde al mes de Junio.', file=month)
        elif num == 7:
            print('El numero ingresado', num, 'corresponde al mes de Julio.', file=month)
        elif num == 8:
            print('El numero ingresado', num, 'corresponde al mes de Agosto.', file=month)
        elif num == 9:
            print('El numero ingresado', num, 'corresponde al mes de Septiembre.', file=month)
        elif num == 10:
            print('El numero ingresado', num, 'corresponde al mes de Octubre.', file=month)
        elif num == 11:
            print('El numero ingresado', num, 'corresponde al mes de Novimbre.', file=month)
        elif num == 12:
            print('El numero ingresado', num, 'corresponde al mes de Diciembre.', file=month)


mes(num)
