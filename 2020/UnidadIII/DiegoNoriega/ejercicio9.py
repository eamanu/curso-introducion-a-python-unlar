"""Aclaración: muevo el puntero seek(i) desde la posición 0 hasta el último valor de la cadena
y siempre leyendo el mismo caracter con - lectura.read(1) - desde donde se posiciona el mismo. Por extraña razón no me toma
el primer caracter - a - siendo que lo inicio desde el principio."""

def virus():
    with open('adn_hebra.txt', 'r') as lectura:
        n = 75
        with open('adn_complementario.txt', 'w') as escritura:
            for i in range(n):
                if lectura.seek(i) and lectura.read(1) == 'a':
                    print('a-t', file=escritura)

                elif lectura.seek(i) and lectura.read(1) == 'c':
                    print('c-g', file=escritura)

                elif lectura.seek(i) and lectura.read(1) == 'g':
                    print('g-c', file=escritura)

                elif lectura.seek(i) and lectura.read(1) == 't':
                    print('t-a', file=escritura)


virus()
