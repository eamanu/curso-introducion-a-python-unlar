#Ejercicio 2.1
def calcular_pares(desde,hasta):
    for i in range(desde,hasta+1):
        if i % 2 == 0:
            with open('ejercicio1.txt', 'a') as f :
                f.write(str(i) + '\t')
print('Comienzo del primer ejercicio')
va1 = int(input('Ingrese desde que valor comenzar: '))
va2 = int(input('Ingrese hasta que valor va a ir: '))
calcular_pares(va1,va2) 
print('Archivo1.txt creado exitosamente!')
print('Comienzo del segundo ejercicio')

#Ejercicio 2.2
def calcular_impares(desde,hasta):
    for i in range(desde,hasta+1):
        if i % 2 != 0:
            with open('ejercicio2.txt', 'a') as f :
                f.write(str(i) + '\t')
va1 = int(input('Ingrese desde que valor comenzar: '))
va2 = int(input('Ingrese hasta que valor va a ir: '))
calcular_impares(va1,va2) 
print('Archivo2.txt creado exitosamente!')

#Ejercicio 2.3
def tabla(valor):
    for i in range(11):
        with open('ejercicio3.txt', 'a') as f :
                f.write(str(valor)+'\tx\t '+ str(i)+ '\t=\t'+str(valor*i)+ '\n')
va1 = int(input('Ingrese el valor de la tabla: '))
tabla(va1)

#Ejercicio 2.4


def calcular_primos(desde,hasta):
    for num in range(desde,hasta+1):
        if num > 1:
            if num == 2: 
                with open('ejercicio4.txt', 'a') as f :
                    f.write(str(num) + '\t')
            else:
                for i in range(2, num): 
                    res = num % i  
                    if res == 0: 
                        con =+1     
                if con==0:
                    with open('ejercicio4.txt', 'a') as f :
                        f.write(str(num) + '\t')
            con = 0 

va1 = int(input('Ingrese desde que valor comenzar: '))
va2 = int(input('Ingrese hasta que valor va a ir: '))


calcular_primos(va1,va2)



#Ejercicio 2.5 
def calcular_pi():
    n = 10000000
    calc = 0
    for i in range(1, n + 1):
        calc += (-1)**(i + 1) / (2 * i - 1)
    resultado = 4 * calc
    with open('ejercicio5.txt', 'w') as f : 
        f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!" + "\n\n" + "El valor del numero PI  es: " +  str(resultado) + "\n\n" + 'FIN')

calcular_pi()
#Ejercicio 2.6
def calcular_descuento(precio,descuento):
    valor_descontar = (precio * descuento) / 100
    precio_descuento = precio - valor_descontar
    with open('ejercicio6.txt', 'w') as f:
        f.write("Se debe cobrar $" + str(precio_descuento) + " con el descuento del " + str(descuento) + "%" )


va1 = int(input('Ingrese Precio ($): '))
va2 = int(input('Ingrese Descuento (%): '))

calcular_descuento(va1,va2)
 
#Ejercicio 2.7 
def seleccionar_mes(valor):
    lista_meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre', 'octubre', 'noviembre','diciembre']
    if (valor >= 1 and valor < 13):
        valor-=1
        print("El mes correspondiente al valor ingresado es: " + lista_meses[valor])      
    else: 
        print('el valor ingresado no corresponde con ningun mes')
va1 = int(input('Ingrese valor:  '))
seleccionar_mes(va1)