def tablaMultiplicar(n=-1):
    if(n==-1):
        n=int(input())
    with open("ejercicio3.txt", 'w') as archivo:
        for i in range(1,10):
            print(n,"x",i," = ",(i*n),sep='\t',file= archivo)