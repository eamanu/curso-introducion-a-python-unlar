def numPI():
    with open("ejercicio5.txt", 'w') as archivo:
        print("Vamos a encontrar el numero PI usando un FOR yeah!!!",'\n',file=archivo)
        n = 10000
        calc=0
        for i in range(1, n + 1):
            calc += (-1)**(i + 1) / (2 * i - 1) 
        resultado = 4 * calc
        print("El valor del numero PI es: "+str(resultado)+'\n',"FIN",sep='\n',file=archivo)