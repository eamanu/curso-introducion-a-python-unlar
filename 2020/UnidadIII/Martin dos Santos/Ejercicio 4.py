def nPrimos(desde=-1,hasta=0):
    if(desde==-1):
        desde = int(input())
    if(hasta==0):
        hasta = int(input())
    with open("ejercicio4.txt", 'w') as archivo:
        for k in range(desde,hasta):
            es_primo= True
            if k > 1:        
                for i in range(2, k):
                    if (k % i) == 0:
                        es_primo=False
                        break
                if(es_primo):
                    archivo.write(str(k)+"\n")

