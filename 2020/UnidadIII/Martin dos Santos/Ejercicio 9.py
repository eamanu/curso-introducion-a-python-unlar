def cadenaVIRUS(letra):
    
    if(letra == "a"):
        letra=letra+"-t"
    elif(letra == "c"):
        letra=letra+"-g"
    elif(letra == "g"):
        letra=letra+"-c"
    elif(letra == "t"):
        letra=letra+"-a"
    else:
        letra="No se encuentra un genoma compatible con " +letra
    return letra

def complementar(file):
    a=file.read()
    with open("adn complementario.txt",'w') as archivo2:
        for i in a:
            archivo2.write(cadenaVIRUS(i)+"\n")

with open("adn_hebra.txt",'r') as archivo1:
    complementar(archivo1)

