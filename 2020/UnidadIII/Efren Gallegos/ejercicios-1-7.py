#funcion ejercicio 1

def pares(hasta):
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio1.txt","w") as e1:
        for x in range(5,hasta):
            if x%2==0:
                e1.write(str(x)+"\n")


#funcion ejercicio 2
def impares(hasta):
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio2.txt","w") as e2:
        for x in range(5,hasta):
            if x%2:
                e2.write(str(x)+"\n")


#funcion ejercicio 3
def tablas(de):
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio3.txt","w") as e3:
        for x in range(11):
            #print(de,"\tX\t", x, "\t=\t", de * x)
            e3.write(str(de)+"\tX\t"+str(x)+"\t=\t"+str(de * x)+"\n")


#funcion ejercicio 4

def primo(desde,hasta):
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio4.txt","w") as e4:

        for x in range(desde,hasta):
            for i in range(1,x+1):
                primo=True
                resto=x%i
                if resto==0 and i!=1 and i!=x:
                    primo=False
                    break
            if primo==True:
                e4.write(str(x)+" es primo\n")


#funcion ejercicio 5
def numpi():
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio5.txt","w") as e5:
        e5.write("Vamos a encontrar el numero PI usando un FOR yeah!!!, end='123'\n\n")
        n = 10000000
        calc = 0
        for i in range(1, n + 1):
            calc += (-1)**(i + 1) / (2 * i - 1)
            resultado = 4 * calc
        e5.write("El valor de PI es: "+str(resultado))
        e5.write("\n\nFIM")

#funcion ejercicio 6
def precio(prod,descu):
    with open("/home/exg22/repositPython/curso-introducion-a-python-unlar/UnidadIII/Efren Gallegos/ejercicio6.txt",
              "w") as e6:
        descuento=prod*descu/100
        e6.write("Se debe cobrar $ "+str(prod-descuento)+" con el descuento de %"+str(descu))

#funcion ejercicio 7

def meses(mes):
    c=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
    print("El mes que ingreso en numero corresponde a ",c[mes-1])
    c=0

#llamamos a las funciones del ejercicio 1 al ejercicio 6 segun la opcion
while True:
    op=int(input("Ingrese el numero de la funcion que quiere usar \n1 - Pares\n2 - Impares\n3 - Tablas de multiplicar\n4 - Numeros primos\n5 - Encontrar el numero PI\n6 - Precio con descuento\n7 - Meses\n8 - Salir\nEsperando su respuesta: "))
    if op == 1:
        p=int(input("Ingrese la cantidad de pares que quiere recibir: "))
        pares(p)
        print("La Salida se escribio en ejercicio1.txt")
    elif op == 2:
        p = int(input("Ingrese la cantidad de pares que quiere recibir: "))
        impares(p)
    elif op == 3:
        p = int(input("De que numero quiere la tabla de multiṕlicar?, ingrese el numero: "))
        tablas(p)
        print("La Salida se escribio en ejercicio3.txt")
    elif op == 4:
        p=0
        q=0
        while p <=2:
            p = int(input("Para realizar esta opecacion debe ingresar desde que numero iniciaran los numeros primos, (siempre que sean mayor o igual 2): "))
        while q <= 2:
            q = int(input("y ahora ingrese hasta donde: "))
        primo(p,q)
        print("La Salida se escribio en ejercicio3.txt")
    elif op == 5:
        numpi()
        print("La Salida se escribio en ejercicio5.txt")
    elif op == 6:
        p = -1
        q = -1
        while p < 0:
            p = int(input("Ingrese precio del producto (siempre que sean mayor a 0): "))
        while q < 0:
            q = int(input("Ingrese el descuento que quiere aplicar(siempre que sean mayor a 0): "))
        precio(p, q)
    elif op == 7:
        p=-1
        while p <= 0 or p >=13:
            p = int(input("Ingrese un numero para saber a que mes se refiere: "))
        meses(p)
    elif op == 8:
        break


