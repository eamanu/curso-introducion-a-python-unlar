print ("Ejercicio 1")
print ("Nros Pares")
desde = int(input("Ingrese numero inicial: "))
hasta = int(input("Ingrese numero final: "))
def nrospares(desde=5,hasta=1000):
    with open ("ejercicio1.txt", 'w') as archivo:
        print("Los numeros pares son: ")
        for i in range(desde,hasta + 1):
            if (i % 2 == 0):
                print (i)            
                archivo.write (str(i))
                archivo.write("\n")
nrospares(desde,hasta)

print ("\nEjercicio 2")
print ("Nros Impares")
desde = int(input("Ingrese numero inicial: "))
hasta = int(input("Ingrese numero final: "))
def nrosimpares (desde=5,hasta=1000): 
    with open ("ejercicio2.txt", 'w') as archivo:
        print ("Nros Impares")
        for i in range(desde,hasta):
            if (i % 2 == 1):
                print (i)
                archivo.write (str(i))
                archivo.write("\n")
nrosimpares(desde,hasta)

print("\nEjercicio 3")
print("Tablas de Multiplicar")
factor1 = int(input("Ingrese Factor: "))
def tablademultiplicar(factor1):
    with open ("ejercicio3.txt", 'w') as archivo:
        print("Tabla del: ", factor1)
        for i in range(11):
            print(factor1, "x ", i, "=", factor1*i, sep='\t')
            print(factor1, "x ", i, "=", factor1*i, sep='\t', file=archivo)
tablademultiplicar(factor1)

print ("\nEjercicio 4")
print ("Muestra los numeros primos en el rango ingresado")
desde = int(input("Ingrese numero inicial: "))
hasta = int(input("Ingrese numero final: "))
def esprimo(desde=1,hasta=101):
    with open ("ejercicio4.txt", 'w') as archivo:
        if desde==1: # Se verifica que sea igual a 1 para descartarlo en el listado.
            desde+=1
        for num in range(desde,hasta):
            primo = True
            for i in range(2,num):
                if (num%i==0):
                    primo = False
            if primo:
                print (num)
                archivo.write (str(num))
                archivo.write("\n")
esprimo(desde,hasta)

print ("\nEjercicio 5 (Calculo NroPI)")
def calculoPI():
    with open ("ejercicio5.txt", 'w') as archivo:
        print("Vamos a encontrar el numero PI usando un FOR yeah!!!\n",file=archivo)
        n = 10000000
        calc = 0
        for i in range(1, n + 1):
            calc += (-1)**(i + 1) / (2 * i - 1) 
        resultado = 4 * calc
        print("El valor del número PI es:",resultado,"\n\nFIN", file=archivo)
calculoPI()

print ("\nEjercicio 6")
print ("Precio / Descuento")
precio = int(input("Ingrese precio: "))
descuento = int(input("Ingrese descuento (En %): "))
def calculadescuento (precio,descuento):
    totaldescuento = (descuento * precio / 100)
    resultado = precio - totaldescuento
    print("Precio Total: $",precio)
    print("Descuento de: ",descuento,"%"," - Total de descuento: $",totaldescuento,"\n\nTotal a cobrar: $",resultado)
    with open ("ejercicio6.txt", 'w') as archivo:
        print("Se debe cobrar $",resultado, "con el descuento de:",descuento,"%",file=archivo)
calculadescuento(precio,descuento)

print ("\nEjercicio 7")
print ("Devuelve un string, evaluando un entero.")
nro_mes = int(input("Ingrese numero de mes: "))
def verifica_mes (nro_mes=1):
    if nro_mes < 1 or nro_mes > 12:
        print("Carga incorrecta. Solo entre 1 y 12")
    else:
        if nro_mes > 6:
            if nro_mes > 10:
                if nro_mes > 11:
                    print("Diciembre")
                else:
                    print("Noviembre")
            else:
                if nro_mes > 8:
                    if nro_mes > 9:
                        print("Octubre")
                    else:
                        print("Septiembre")
                else:
                    if nro_mes > 7:
                        print("Agosto")
                    else:
                        print("Julio")
        else:
            if nro_mes>3:
                if nro_mes>5:
                    print("Junio")
                else:
                    if nro_mes>4:
                        print("Mayo")
                    else:
                        print("Abril")
            else:
                if nro_mes>2:
                    print("Marzo")
                else:
                    if nro_mes >1:
                        print("Febrero")
                    else:
                        print("Enero")
verifica_mes(nro_mes)

'''Se uso para verificar que solo acepte valores entre 1 y 12 y que su devolucion sea la correcta
for i in range(14):
    verifica_mes(i)'''