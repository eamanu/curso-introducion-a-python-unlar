# ------------NUMEROS PARES------------
def num_pares(desde=1, hasta=1001):
    cont = 0
    with open("ejercicio1.txt", "w") as f:
        for i in range(desde, (hasta + 1)):
            if i % 2 == 0:
                cont = cont + 1
                print(i, end='\t')
                f.write(str(i) + "\n")
                if cont == 15:
                    cont = 0
                    print("\n")
    print("\n")


# -------------------------------------


# -----------NUMEROS IMPARES-----------
def num_impares(desde=5, hasta=1001):
    cont = 0
    with open("ejercicio2.txt", "w") as f:
        for i in range(desde, (hasta + 1)):
            if i % 2 != 0:
                cont = cont + 1
                print(i, end='\t')
                f.write(str(i)+"\n")
                if cont == 15:
                    cont = 0
                    print("\n")
    print("\n")


# -------------------------------------


# -----------TABLA DEL NUEVE-----------
def cacular_tabla(num):
    with open("ejercicio3.txt", "w") as f:
        for i in range(0, 11):
            print(num, "x", str(i), "=", str(i * 9), sep='\t')
            f.write(str(num)+"\t" + "x"+"\t"+ str(i)+"\t" +"="+"\t"+ str(i * 9)+"\n")

# -------------------------------------


# -----------INTERSECCIONES-----------
def intersecciones():
    cantidad = 0
    for i in range(201):
        for j in range(67, 101):
            if i == j:
                cantidad = cantidad + 1
                print("i=" + str(i) + " j=" + str(j) + " interseccion Nº " + str(cantidad))
    print("\nCANTIDAD DE INTERSECCIONES----------> " + str(cantidad))


# -------------------------------------


# -----------NUMEROS PRIMOS-----------
def num_primos(desde, hasta):
    with open("ejercicio4.txt", "w") as f:
        for i in range(desde, hasta + 1):
            bandera_primo = True
            if (i >= 2):
                for j in range(2, i):
                    if i % j == 0:
                        bandera_primo = False
                        break
                if bandera_primo:
                    print(i, " es un primo\n")
                    f.write(str(i)+"\n")



# -------------------------------------


# -------------NUMERO PI---------------
def numero_pi():
    print("Vamos a encontrar el numero PI usando un FOR yeah!!! \n")

    n = 10000000
    # x = 0 <--------Esta variable no se usa
    calc = 0

    for i in range(1, n + 1):
        calc += (-1) ** (i + 1) / (2 * i - 1)

    resultado = 4 * calc
    print("El valor del número PI es: " + str(resultado) + "\n \n" + "FIN")

    with open("ejercicio5.txt", "w") as f:
        f.write("Vamos a encontrar el numero PI usando un FOR yeah!!!\n\nEl valor del numero PI es: " + str(resultado)+"\n\nFIN")

# -------------------------------------


# ---------PRECIO CON DESCUENTO-------
def precio_descuento(precio, descuento):
    total = precio - ((descuento * precio) / 100)
    print("Se debe cobrar $"+str(total)+" con el"+"descuento de "+str((descuento * precio) / 100)+"%")
    with open("ejercicio6.txt", "w") as f:
        f.write("Se debe cobrar $"+str(total)+" con el"+"descuento de "+str((descuento * precio) / 100)+"%")

# -------------------------------------


# ---------MESES POR NUMERO------------
def meses_x_numero(numero):
    meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
             "Noviembre", "Diciembre"]
    with open("ejercicio7.txt", "w") as f:
        if numero > 0 and numero < 13:
            print(meses[numero - 1])
            mes=meses[numero - 1]
            f.write(mes)
        else:
            print("Fuera de rango")
            f.write("Fuera de rango")


# -------------------------------------


# ---------FUNCION PARA SELECCIONAR----
def funciones():
    print("""        
Elige una de las siguientes funciones presionado su correspondiente numero:
1-Numeros pares
2-Numeros impares
3-Tabla de multiplicar
4-intersecciones <----------------Me olvide de sacarlo disculpe
5-Numeros Primos
6-Numero pi
7-Precio-descuento
8-Meses por numero


            """)
    opcion1 = int(input("elige una Opcion: "))
    opcion2 = 0
    while opcion2 != -1:
        if opcion1 == 1:
            print("1 - para introducir rango de numeros pares \n2 - para escribir numeros pares desde 1 a 1000 ")
            if int(input())==1:
                num_pares(int(input("desde: ")), int(input("hasta: ")))
            else:
                num_pares()
        if opcion1 == 2:
            print("1- para introducir rango de numeros impares o 2 - para escribir numeros impares desde 1 a 1000 ")
            desde = int(input("desde: "))
            hasta = int(input("hasta: "))
            num_impares(desde, hasta)
        if opcion1 == 3:
            num_de_tabla=int(input("Introducir un numero para seleccionar una tabla de multiplicar: "))
            cacular_tabla(num_de_tabla)
        if opcion1 == 4:
            intersecciones()
        if opcion1 == 5:
            print("Introduce rango de numeros primos")
            desde = int(input("desde: "))
            hasta = int(input("hasta: "))
            num_primos(desde, hasta)
        if opcion1 == 6:
            numero_pi()
        if opcion1 == 7:
            precio = float(input("Introduce precio: "))
            descuento = float(input("Introduce descuento: "))
            precio_descuento(precio, descuento)
        if opcion1 == 8:
            numero = int(input("Por Favor Seleccione un numero de mes "))
            meses_x_numero(numero)

        opcion2 = int(input("Presiona 1 para ver de nuevo las opciones o presiona -1 para salir: "))

        if opcion2 == 1:
            print("""        
Elige una de las siguientes funciones presionado su correspondiente numero:
1-Numeros pares
2-Numeros impares
3-Tablas de multiplicar
4-intersecciones <----------------Me olvide de sacarlo disculpe
5-Numeros Primos
6-Numero pi
7-Precio-descuento
8-Meses por numero

                        """)
            opcion1 = int(input("elige una Opcion: "))

    else:
        print("---->Programa Finalizado")


# ------------------------------------------------------------------------------------

funciones()
