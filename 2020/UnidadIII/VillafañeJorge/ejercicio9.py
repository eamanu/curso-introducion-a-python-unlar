
def listarParesBasesNitrogenadas(cadena_de_adn="adn_hebra.txt"):
    with open(cadena_de_adn, "r") as f:
        list_Hebra = ""
        list_Hebra = f.read()
        contA=0
        contT = 0
        contC = 0
        contG = 0

        for i in list_Hebra:
            if i=="a":
                contA +=1
            if i=="t":
                contT+=1
            if i=="c":
                contC+=1
            if i=="g":
                contG+=1
        with open("adn complementario.txt", "w") as archivo:
            for a in range(contA):
                print("a-t")
                archivo.write("a-t\n")
            for c in range(contC):
                print("c-g")
                archivo.write("c-g\n")
            for g in range(contG):
                print("g-c")
                archivo.write("g-c\n")
            for t in range(contT):
                print("t-a")
                archivo.write("t-a\n")


def funcion_captura_de_datos():
    print("Escribir 1 para usar la hebra predeterminada: 'adn_hebra.txt'\nEscribe 2 para introducir direccion de cadena de adn")
    opcion=int(input())

    if opcion==1:
        listarParesBasesNitrogenadas()
    else:
        cadena = input("introduce el nombre de la cadena, en caso de no estar en la misma carpta,\nintroducir direccion ejemplo 'C:/Users/Administrador/Downloads/Documents/cadena.txt':\n")
        listarParesBasesNitrogenadas(cadena)


funcion_captura_de_datos()

