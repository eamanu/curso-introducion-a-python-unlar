from http import client


def funcion_http(var_host, var_puerto=80, var_timeout=10):
    con = client.HTTPConnection(var_host, int(var_puerto), timeout=int(var_timeout))
    con.request("GET", "/")
    response_pagina = con.getresponse()
    if response_pagina.status==200:
        print("Status code:",response_pagina.status," Reason phrase:", response_pagina.reason," Response body:", response_pagina.read())
    else:
        print(str(var_host)+" está caido? Error: ", response_pagina.reason)


def funcion_captura_de_datos():
    print("Escribe un 1 para definir host, puerto y timeOut\nEscribe 2 para definir solo el host\n-->")
    opcion=int(input())
    if  opcion==1:
        host=input("Escribir host ejemplo 'www.ejemplo.com': ")
        puerto=int(input("Escribir puerto ejemplo '80': "))
        timeOut=int(input("Escribir timeOut ejemplo '10': "))
        funcion_http(host,puerto,timeOut)
    elif opcion==2:
        host=input("Escribir host ejemplo 'www.ejemplo.com': ")
        funcion_http(host)


funcion_captura_de_datos()
